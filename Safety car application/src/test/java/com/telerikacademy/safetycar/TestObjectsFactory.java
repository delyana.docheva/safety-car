package com.telerikacademy.safetycar;


import com.telerikacademy.safetycar.models.*;

import java.time.LocalDate;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.enums.AuthorityType;
import com.telerikacademy.safetycar.models.enums.PaymentMethodType;
import com.telerikacademy.safetycar.models.enums.PaymentStatusType;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.models.verification.VerificationToken;

import java.time.LocalDate;
import java.util.*;


public final class TestObjectsFactory {
    private static final String EMAIL = "test@gmail.com";
    private static final String EMAIL_AGENT = "agent@gmail.com";
    private static final String EMAIL_ADMIN = "admin@gmail.com";
    private static final String PASSWORD = "Password";
    private static final String FIRST_NAME = "John";
    private static final String LAST_NAME = "Hopkins";
    private static final String PHONE = "0885164550";
    private static final String ADDRESS = "Some nice Str.";
    private static final String CAR_NUMBER = "CO5623TA";

    private TestObjectsFactory() {
    }

    public static User createUser() {
        User user = new User();
        user.setId(1);
        user.setUsername(EMAIL);
        user.setPassword(PASSWORD);
        user.setEnabled(true);
        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(user.getId(), user.getUsername(), AuthorityType.ROLE_USER.name()));
        user.setAuthorities(authorities);
        return user;
    }

    public static User createAgent() {
        User user = new User();
        user.setId(1);
        user.setUsername(EMAIL_AGENT);
        user.setPassword(PASSWORD);
        user.setEnabled(true);
        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(user.getId(), user.getUsername(), AuthorityType.ROLE_AGENT.name()));
        user.setAuthorities(authorities);
        return user;
    }

    public static User createAdmin() {
        User user = new User();
        user.setId(1);
        user.setUsername(EMAIL_ADMIN);
        user.setPassword(PASSWORD);
        user.setEnabled(true);
        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(user.getId(), user.getUsername(), AuthorityType.ROLE_ADMIN.name()));
        user.setAuthorities(authorities);
        return user;
    }

    public static VerificationToken createVerificationToken(User user) {
        VerificationToken token = new VerificationToken();
        token.setUser(user);
        token.setToken(UUID.randomUUID().toString());
        return token;
    }

    public static PasswordResetToken createPasswordToken(User user) {
        PasswordResetToken token = new PasswordResetToken();
        token.setUser(user);
        token.setToken(UUID.randomUUID().toString());
        return token;
    }

    public static Car createCar() {
        Car car = new Car();
        car.setId(1);
        car.setCubicCapacity(1000);
        car.setRegistrationDate(LocalDate.now());
        car.setModel(createModel());
        car.setOwner(createUser());
        car.setRegistrationNumber(CAR_NUMBER);
        return car;
    }

    public static CarModel createModel() {
        CarModel model = new CarModel();
        model.setId(1);
        model.setBrand(createBrand());
        model.setName("safetyCarModel");
        return model;
    }

    public static CarBrand createBrand() {
        CarBrand brand = new CarBrand();
        brand.setId(1);
        brand.setName("safetyCarBrand");
        return brand;
    }

    public static InsurancePolicy createPolicy() {
        InsurancePolicy policy = new InsurancePolicy();
        policy.setId(1);
        policy.setOwner(createUser());
        policy.setPremiumAmount(10.0);
        policy.setCar(createCar());

        policy.setStatus(createPolicyStatus(PolicyStatusType.GENERATED, 1));

        policy.setStartDate(LocalDate.now());
        policy.setExpiringDate(LocalDate.now().plusMonths(12).minusDays(1));

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setType(PaymentMethodType.PREPAID);
        paymentMethod.setId(1);
        policy.setPaymentMethod(paymentMethod);
        policy.setPaymentStatus(createPaymentStatusPending());
        return policy;
    }

    public static PaymentMethod createPaymentMethodExtended() {
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setType(PaymentMethodType.EXTENDED);
        paymentMethod.setId(2);
        return paymentMethod;
    }

    public static PolicyStatus createPolicyStatus(PolicyStatusType type, int id) {
        PolicyStatus status = new PolicyStatus();
        status.setType(type);
        status.setId(id);
        return status;
    }

    public static PaymentStatus createPaymentStatusPending() {
        PaymentStatus status = new PaymentStatus();
        status.setId(1);
        status.setType(PaymentStatusType.PENDING);
        return status;
    }

    public static PaymentStatus createPaymentStatusComplete() {
        PaymentStatus status = new PaymentStatus();
        status.setId(1);
        status.setType(PaymentStatusType.COMPLETE);
        return status;
    }

    public static List<InsurancePolicy> createPoliciesList() {
        InsurancePolicy policy2 = new InsurancePolicy();
        policy2.setStartDate(LocalDate.now());
        policy2.setExpiringDate(LocalDate.now().plusMonths(12).minusDays(1));
        policy2.setPaymentStatus(createPaymentStatusComplete());
        policy2.setStatus(createPolicyStatus(PolicyStatusType.GENERATED, 1));
        List<InsurancePolicy> allPolicies = new ArrayList<>();
        allPolicies.add(policy2);
        allPolicies.add(createPolicy());
        return allPolicies;
    }

    public static Country createCountry() {
        Country country = new Country();
        country.setId(1);
        country.setName("Bulgaria");
        return country;
    }

    public static District createDistrict() {
        District district = new District();
        district.setId(1);
        district.setName("Sofia");
        district.setCountry(createCountry());
        return district;
    }

    public static City createCity() {
        City city = new City();
        city.setId(1);
        city.setName("Sofia");
        city.setDistrict(createDistrict());
        return city;
    }
}
