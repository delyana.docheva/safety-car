package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.Country;
import com.telerikacademy.safetycar.models.District;
import com.telerikacademy.safetycar.repositories.CountryRepository;
import com.telerikacademy.safetycar.repositories.DistrictRepository;
import com.telerikacademy.safetycar.services.CountryServiceImpl;
import com.telerikacademy.safetycar.services.DistrictServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.createCountry;

@ExtendWith(MockitoExtension.class)
public class CountryServiceTest {
    @InjectMocks
    private CountryServiceImpl countryService;

    @Mock
    private CountryRepository mockCountryRepository;

    private Country testCountry;
    private List<Country> testListCountries;

    @BeforeEach
    public void initiate() {
        testCountry = createCountry();
        testListCountries = Collections.singletonList(testCountry);
    }

    @Test
    public void getAll_ShouldReturnListOfCountries() {
        //Arrange
        Mockito.when(mockCountryRepository.getAll())
                .thenReturn(testListCountries);

        //Act
        List<Country> listToCompare = countryService.getAll();

        //Assert
        Assertions.assertEquals(testListCountries, listToCompare);
    }

}
