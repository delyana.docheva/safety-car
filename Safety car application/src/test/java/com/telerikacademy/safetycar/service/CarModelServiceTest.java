package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.repositories.CarModelRepository;
import com.telerikacademy.safetycar.services.CarModelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.createModel;

@ExtendWith(MockitoExtension.class)
public class CarModelServiceTest {
    @InjectMocks
    private CarModelServiceImpl carService;

    @Mock
    private CarModelRepository mockCarRepository;

    private CarModel testModel;
    private List<CarModel> testListModels;

    @BeforeEach
    public void initiate() {
        testModel = createModel();
        testListModels = Collections.singletonList(testModel);
    }

    @Test
    public void getById_ShouldReturnModel_WhenModelExists() {
        //Arrange
        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenReturn(testModel);

        //Act
        CarModel modelToCompare = carService.getById(testModel.getId());

        //Assert
        Assertions.assertEquals(testModel.getId(), modelToCompare.getId());
        Assertions.assertEquals(testModel.getName(), modelToCompare.getName());
        Assertions.assertEquals(testModel.getBrand(), modelToCompare.getBrand());
    }

    @Test
    public void getByBrandId_ShouldReturnListOfModels_WhenModelsExists() {
        //Arrange
        Mockito.when(mockCarRepository.getByBrandId(Mockito.anyInt()))
                .thenReturn(testListModels);

        //Act
        List<CarModel> modelsListToCompare = carService.getByBrandId(testModel.getBrand().getId());

        //Assert
        Assertions.assertEquals(testListModels, modelsListToCompare);
    }



}
