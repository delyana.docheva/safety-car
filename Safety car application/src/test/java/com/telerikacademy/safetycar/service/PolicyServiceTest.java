package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.exceptions.AccessDeniedException;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.enums.CoefficientType;
import com.telerikacademy.safetycar.models.enums.PaymentStatusType;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.repositories.*;
import com.telerikacademy.safetycar.services.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.safetycar.TestObjectsFactory.*;

@ExtendWith(MockitoExtension.class)
public class PolicyServiceTest {

    @InjectMocks
    private InsurancePolicyServiceImpl policyService;

    @InjectMocks
    private PolicyStatusServiceImpl policyStatusService;

    @Mock
    private InsurancePolicyRepositoryImpl mockPolicyRepository;

    @Mock
    private UserService mockUserService;

    @Mock
    private PolicyStatusRepository mockPolicyStatusRepository;

    @Mock
    private PaymentStatusRepository mockPaymentStatusRepository;

    @Mock
    private PremiumCalculationRepository mockPremiumCalculationRepository;

    @Mock
    private CarService mockCarService;

    @Mock
    private CarRepository carRepository;

    @Mock
    private EmailService mockEmailService;

    @Captor
    ArgumentCaptor<InsurancePolicy> policyCaptor;


    private InsurancePolicy policy;
    private User user;
    private User agent;
    private PolicyStatus policyStatusGenerated;
    private PolicyStatus policyStatusActive;
    private PolicyStatus policyStatusExpired;
    private PaymentStatus paymentStatus;
    private List<InsurancePolicy> insurancePoliciesList;
    private List<PolicyStatus> policyStatusList;

    @BeforeEach
    void initiate() {
        policy = createPolicy();
        user = createUser();
        agent = createAgent();
        insurancePoliciesList = createPoliciesList();
        paymentStatus = createPaymentStatusPending();
        policyStatusGenerated = createPolicyStatus(PolicyStatusType.GENERATED, 1);
        policyStatusActive = createPolicyStatus(PolicyStatusType.ACTIVE, 7);
        policyStatusExpired = createPolicyStatus(PolicyStatusType.EXPIRED, 8);
        policyStatusList = Collections.singletonList(policyStatusGenerated);
    }

    @Test
    public void getById_ShouldReturn_InsurancePolicy_WhenMatchExist() {
        //Arrange
        Mockito.when(mockPolicyRepository.getById(1)).thenReturn(Optional.ofNullable(policy));

        //Act
        InsurancePolicy result = policyService.getById(1);

        //Assert
        Assertions.assertEquals(policy, result);
    }

    @Test
    public void getById_ShouldThrow_WhenMatchNotExist() {
        //Arrange
        Mockito.when(mockPolicyRepository.getById(1)).thenReturn(Optional.empty());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> policyService.getById(1));
    }

    @Test
    public void getAll_ShouldReturn_AllInsurancePolicies() {
        //Arrange
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(createPoliciesList());

        //Act
        List<InsurancePolicy> result = policyService.getAll();

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).getAll();
        Assertions.assertEquals(2, result.size());
    }

    @Test
    public void getPoliciesForUser_ShouldReturn_AllPoliciesOfUserByUsername() {
        //Arrange
        Mockito.when(mockUserService.getByUsername(user.getUsername())).thenReturn(user);
        List<InsurancePolicy> allUserPolicies = createPoliciesList();
        Mockito.when(mockPolicyRepository.getPoliciesForUser(user.getId())).thenReturn((allUserPolicies));

        //Act
        List<InsurancePolicy> result = policyService.getPoliciesForUser(user.getUsername());

        //Assert
        Assertions.assertEquals(allUserPolicies.size(), result.size());
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).getPoliciesForUser(user.getId());
    }

    @Test
    public void requestPolicy_Should_UpdatePolicy() {
        //Arrange
        Mockito.when(mockPolicyStatusRepository.getByType(PolicyStatusType.REQUESTED)).thenReturn(createPolicyStatus(PolicyStatusType.REQUESTED, 2));
        Mockito.when(mockPaymentStatusRepository.getByType(PaymentStatusType.PENDING)).thenReturn(createPaymentStatusPending());
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(Optional.of(policy));
        double originalPremium = policy.getPremiumAmount();
        user.setHasAccident(true);
        Mockito.when(mockUserService.getByUsername(Mockito.anyString())).thenReturn(user);

        //Act
        policyService.requestPolicy(policy, policy.getOwner().getUsername());

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).update(policyCaptor.capture());
        InsurancePolicy updated = policyCaptor.getValue();
        Assertions.assertEquals(PolicyStatusType.REQUESTED, updated.getStatus().getType());
        Assertions.assertEquals(PaymentStatusType.PENDING, updated.getPaymentStatus().getType());
        Assertions.assertEquals(originalPremium, updated.getPremiumAmount());
    }

    @Test
    public void requestPolicy_Should_IncreasePolicyPremium_WhenPaymentMethodIsExtended() {
        //Arrange
        Mockito.when(mockPolicyStatusRepository.getByType(PolicyStatusType.REQUESTED)).thenReturn(createPolicyStatus(PolicyStatusType.REQUESTED, 2));
        Mockito.when(mockPaymentStatusRepository.getByType(PaymentStatusType.PENDING)).thenReturn(createPaymentStatusPending());
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(Optional.of(policy));
        double originalPremium = policy.getPremiumAmount();
        Mockito.when(mockPremiumCalculationRepository.getCoefficient(CoefficientType.PAYMENT_METHOD_COEFF)).thenReturn(1.1);
        user.setHasAccident(true);
        Mockito.when(mockUserService.getByUsername(Mockito.anyString())).thenReturn(user);
        policy.setPaymentMethod(createPaymentMethodExtended());


        //Act
        policyService.requestPolicy(policy, policy.getOwner().getUsername());

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).update(policyCaptor.capture());
        InsurancePolicy updated = policyCaptor.getValue();
        Assertions.assertEquals(originalPremium * 1.1, updated.getPremiumAmount());
    }

    @Test
    public void requestPolicy_ShouldThrow_WhenPolicyAlreadyRequested() {
        //Arrange
        policy.setStatus(createPolicyStatus(PolicyStatusType.PENDING, 3));
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(Optional.of(policy));

        //Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> policyService.requestPolicy(policy, Mockito.anyString()));
    }

    @Test
    public void update_Should_UpdatePolicy_WhenLoggedUserIsOwner() {
        //Act
        policyService.update(policy, policy.getOwner().getUsername());

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).update(policy);
    }

    @Test
    public void update_ShouldThrow_WhenLoggedUserNotOwner() {
        //Arrange
        Mockito.when(mockUserService.getByUsername(Mockito.any()))
                .thenReturn(user);

        //Act, Assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> policyService.update(policy, Mockito.anyString()));
    }

    @Test
    public void delete_ShouldDelete_WhenPolicyExistsAndLoggedUserIsOwner() {
        //Arrange
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(Optional.ofNullable(policy));

        //Act
        policyService.delete(policy.getId(), policy.getOwner().getUsername());

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).delete(policy);
    }

    @Test
    public void delete_ShouldThrow_WhenPolicyExistsAndLoggedUserNotOwner() {
        //Arrange
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(Optional.ofNullable(policy));

        //Act, Assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> policyService.delete(policy.getId(), Mockito.anyString()));
    }

    @Test
    public void delete_ShouldThrow_WhenPolicyNotExist() {
        //Arrange
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt())).thenReturn(Optional.empty());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> policyService.delete(policy.getId(), Mockito.anyString()));
    }

    @Test
    public void save_Should_SavePolicy_WithCorrectStatusOwnerAndCar() {
        //Arrange
        Mockito.when(mockUserService.getActiveById(user.getId())).thenReturn(user);
        InsurancePolicy toSave = new InsurancePolicy();
        Mockito.when(mockPolicyStatusRepository.getByType(PolicyStatusType.GENERATED)).thenReturn(createPolicyStatus(PolicyStatusType.GENERATED, 1));
        Car car = new Car();
        car.setRegistrationNumber("1");
        toSave.setCar(car);
        Mockito.when(mockCarService.getByRegistrationNumber(car.getRegistrationNumber())).thenReturn(car);
        Mockito.when(mockPolicyRepository.save(toSave)).thenReturn(1);

        //Act
        policyService.save(toSave, user);

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).save(policyCaptor.capture());
        InsurancePolicy saved = policyCaptor.getValue();
        Assertions.assertEquals(user, saved.getOwner());
        Assertions.assertEquals(PolicyStatusType.GENERATED, saved.getStatus().getType());
        Assertions.assertEquals(car, saved.getCar());
    }

    @Test
    public void save_ShouldThrow_WhenOwnerDoesNotExist() {
        //Arrange
        Mockito.when(mockUserService.getActiveById(user.getId())).thenThrow(new EntityNotFoundException(Mockito.anyString()));
        InsurancePolicy toSave = new InsurancePolicy();

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> policyService.save(toSave, user));
    }

    @Test
    public void getAllRequestedPolicies_Should_GetAllRequestedPolicies() {
        //Act
        policyService.getAllRequestedPolicies();

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).getAllRequestedPolicies();
    }

    @Test
    public void scheduleCheckOfExpiringPolicies_ShouldSendEmail_WhenPolicyIsAboutToExpire() {
        //Arrange
        Mockito.when(mockPolicyRepository.getAllActive()).thenReturn(insurancePoliciesList);
        insurancePoliciesList.get(0).setExpiringDate(LocalDate.now().plusDays(1));

        //Act
        policyService.scheduleCheckOfExpiringPolicies();

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).getAllActive();
        Mockito.verify(mockEmailService, Mockito.times(1))
                .emailUserForSoonExpiringPolicy(insurancePoliciesList.get(0));

    }

    @Test
    public void scheduleStatusUpdate_ShouldUpdateStatus_OnPolicyStartDate() {
        //Arrange
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(insurancePoliciesList);
        Mockito.when(mockPolicyStatusRepository.getByType(Mockito.any())).thenReturn(policyStatusActive);
        InsurancePolicy policyToUpdate = insurancePoliciesList.get(0);
        policyToUpdate.getStatus().setType(PolicyStatusType.APPROVED);
        policyToUpdate.setExpiringDate(LocalDate.now().plusMonths(12).minusDays(1));
        policyToUpdate.getPaymentStatus().setType(PaymentStatusType.COMPLETE);

        //Act
        policyService.scheduleStatusUpdate();

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).getAll();
        Mockito.verify(mockEmailService, Mockito.times(1))
                .emailUserForPolicyActivation(insurancePoliciesList.get(0));
        Assertions.assertEquals(policyToUpdate.getStatus().getType(), PolicyStatusType.ACTIVE);

    }

    @Test
    public void scheduleStatusUpdate_ShouldUpdateStatus_OnPolicyExpiringDate() {
        //Arrange
        Mockito.when(mockPolicyRepository.getAll()).thenReturn(insurancePoliciesList);
        Mockito.when(mockPolicyStatusRepository.getByType(Mockito.any())).thenReturn(policyStatusExpired);
        InsurancePolicy policyToUpdate = insurancePoliciesList.get(0);
        policyToUpdate.setExpiringDate(LocalDate.now());

        //Act
        policyService.scheduleStatusUpdate();

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).getAll();
        Mockito.verify(mockEmailService, Mockito.times(1))
                .emailUserForExpiredPolicy(insurancePoliciesList.get(0));
        Assertions.assertEquals(policyToUpdate.getStatus().getType(), PolicyStatusType.EXPIRED);

    }

    @Test
    public void getAll_ShouldReturn_ListOfPolicyStatus() {
        //Arrange
        Mockito.when(mockPolicyStatusRepository.getAll())
                .thenReturn(policyStatusList);

        //Act
        List<PolicyStatus> listToCompare = policyStatusService.getAll();

        //Assert
        Assertions.assertEquals(policyStatusList, listToCompare);
    }

    @Test
    public void changePolicyStatus_Should_UpdatePolicy() {
        //Arrange
        Mockito.when(mockPolicyStatusRepository.getByType(PolicyStatusType.REQUESTED)).thenReturn(createPolicyStatus(PolicyStatusType.REQUESTED, 2));
        double originalPremium = policy.getPremiumAmount();
        user.setHasAccident(true);
        Mockito.when(mockUserService.getByUsername(Mockito.anyString())).thenReturn(agent);

        //Act
        policyService.changePolicyStatus(PolicyStatusType.REQUESTED, policy, agent.getUsername());

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).update(policyCaptor.capture());
        InsurancePolicy updated = policyCaptor.getValue();
        Assertions.assertEquals(PolicyStatusType.REQUESTED, updated.getStatus().getType());
        Assertions.assertEquals(PaymentStatusType.PENDING, updated.getPaymentStatus().getType());
        Assertions.assertEquals(originalPremium, updated.getPremiumAmount());
    }

    @Test
    public void calculateExpectedPremium_Should_ReturnPremium() {
        //Arrange
        policy.getCar().setRegistrationDate(LocalDate.now().minusYears(10));
        int carAge = policyService.calculateAge(policy.getCar().getRegistrationDate());
        policy.getOwner().setBirthDate(LocalDate.now().minusYears(20));
        policy.getOwner().setHasAccident(true);
        Mockito.when(mockPremiumCalculationRepository.getBaseAmount(policy.getCar().getCubicCapacity(), carAge)).thenReturn(403.25);
        Mockito.when(mockPremiumCalculationRepository.getCoefficient(CoefficientType.TAX_COEFF)).thenReturn(1.1);
        Mockito.when(mockPremiumCalculationRepository.getCoefficient(CoefficientType.DRIVER_AGE_COEFF)).thenReturn(1.05);
        Mockito.when(mockPremiumCalculationRepository.getCoefficient(CoefficientType.ACCIDENT_COEFF)).thenReturn(1.2);
        Mockito.when(mockPremiumCalculationRepository.getCoefficient(CoefficientType.MIN_DRIVER_AGE_FOR_DISCOUNT)).thenReturn(25.0);
        double premiumToCompare = 403.25*1.05*1.2*1.1;
        double scale = Math.pow(10, 4);
        premiumToCompare = Math.round(premiumToCompare * scale) / scale;

        //Act
        double actualAmount = policyService.calculateExpectedPremium(policy);

        //Assert
        Assertions.assertEquals(actualAmount, premiumToCompare);
    }

}
