package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.exceptions.*;
import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.models.verification.VerificationToken;
import com.telerikacademy.safetycar.repositories.AuthorityRepository;
import com.telerikacademy.safetycar.repositories.InsurancePolicyRepository;
import com.telerikacademy.safetycar.repositories.InsurancePolicyRepositoryImpl;
import com.telerikacademy.safetycar.repositories.UserRepositoryImpl;
import com.telerikacademy.safetycar.services.EmailService;
import com.telerikacademy.safetycar.services.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepositoryImpl mockUserRepository;

    @Mock
    private AuthorityRepository mockAuthorityRepository;

    @Mock
    private InsurancePolicyRepository mockPolicyRepository;

    @Mock
    private EmailService mockEmailService;

    private User testUser;
    private User otherUser;
    private User testAdmin;
    private List<User> userList;
    private VerificationToken testVerificationToken;
    private PasswordResetToken testPasswordToken;

    @BeforeEach
    void initiate() {
        testUser = createUser();
        otherUser = createUser();
        otherUser.setUsername("user@gmail.com");
        testAdmin = createAdmin();
        testVerificationToken = createVerificationToken(testUser);
        testPasswordToken = createPasswordToken(testUser);
    }

    @Test
    public void create_ShouldThrow_WhenUserExists() {
        //Arrange
        Mockito.when(mockUserRepository.existsByName(Mockito.any()))
                .thenReturn(true);

        //Act, assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(testUser));
        Mockito.verify(mockUserRepository,
                Mockito.times(0)).create(testUser);
    }

    @Test
    public void loadUserByUsername_ShouldReturnUserDetails_WhenUserExists() {
        //Arrange
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testUser);
        GrantedAuthority[] authorities = new SimpleGrantedAuthority[1];
        authorities[0] = new SimpleGrantedAuthority(testUser.getAuthorities().toString());
        org.springframework.security.core.userdetails.User springUser =
                new org.springframework.security.core.userdetails.User(testUser.getUsername(), testUser.getPassword(),
                        true, true, true, true,
                        Arrays.asList(authorities));

        //Act
        UserDetails userToCompare = userService.loadUserByUsername(testUser.getUsername());

        //Assert
        Assertions.assertEquals(testUser.getUsername(), userToCompare.getUsername());
    }

    @Test
    public void create_ShouldCreateUser_WhenInputIsValid() {
        //Arrange
        Mockito.when(mockUserRepository.existsByName(Mockito.any()))
                .thenReturn(false);

        //Act
        userService.create(testUser);

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).create(testUser);
    }

    @Test
    public void update_ShouldUpdate_WhenInputIsValid() {
        //Arrange
        Mockito.when(mockUserRepository.existsByName(Mockito.any()))
                .thenReturn(false);
        Mockito.when(mockPolicyRepository.getPoliciesForUser(testUser.getId()))
                .thenReturn(new ArrayList<>());
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("test@gmail.com");

        //Act
        userService.update(testUser, mockPrincipal.getName());

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
    }

    @Test
    public void update_ShouldThrow_WhenUserIsNotAuthorised() {
        //Arrange
        Mockito.when(mockUserRepository.getByUsername(Mockito.any()))
                .thenReturn(otherUser);
        Mockito.when(mockPolicyRepository.getPoliciesForUser(testUser.getId()))
                .thenReturn(new ArrayList<>());
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn(otherUser.getUsername());

        //Act, Assert
        Assertions.assertThrows(UnauthorizedException.class,
                () -> userService.update(testUser, mockPrincipal.getName()));
        Mockito.verify(mockUserRepository,
                Mockito.times(0)).update(testUser);
    }

    @Test
    public void update_ShouldThrow_WhenUsernameIsTaken() {
        //Arrange
        Mockito.when(mockUserRepository.existsByName(Mockito.any()))
                .thenReturn(true);
        Mockito.when(mockUserRepository.getByUsername(Mockito.any()))
                .thenReturn(otherUser);
        testUser.setId(1);
        otherUser.setId(2);
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn(testUser.getUsername());

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(testUser, mockPrincipal.getName()));
        Mockito.verify(mockUserRepository,
                Mockito.times(0)).update(testUser);
    }

    @Test
    public void updatePassword_ShouldUpdatePassword() {
        //Arrange
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("test@gmail.com");

        //Act
        userService.updatePassword(testUser, mockPrincipal);

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
    }

    @Test
    public void updatePassword_ShouldThrow_WhenPrincipleIsNotValid() {
        //Arrange
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("principle@gmail.com");

        //Act, assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> userService.updatePassword(testUser, mockPrincipal));
        Mockito.verify(mockUserRepository,
                Mockito.times(0)).update(testUser);
    }

    @Test
    public void getById_ShouldReturn_WhenUserExists() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(testUser);

        //Act
        User userToCompare = userService.getById(testUser.getId());

        //Assert
        Assertions.assertEquals(testUser.getId(), userToCompare.getId());
        Assertions.assertEquals(testUser.getUsername(), userToCompare.getUsername());
    }

    @Test
    public void getActiveById_ShouldReturn_WhenUserExists() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(testUser);

        //Act
        User userToCompare = userService.getActiveById(testUser.getId());

        //Assert
        Assertions.assertEquals(testUser.getId(), userToCompare.getId());
        Assertions.assertEquals(testUser.getUsername(), userToCompare.getUsername());
    }

    @Test
    public void getActiveById_ShouldThrow_WhenUserNotFound() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(testUser);
        testUser.setEnabled(false);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getActiveById(testUser.getId()));
    }

    @Test
    public void getByUsername_ShouldReturn_WhenUserExists() {
        //Arrange
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testUser);

        //Act
        User userToCompare = userService.getByUsername(testUser.getUsername());

        //Assert
        Assertions.assertEquals(testUser.getId(), userToCompare.getId());
        Assertions.assertEquals(testUser.getUsername(), userToCompare.getUsername());
    }

    @Test
    public void getByUsername_ShouldThrow_WhenUserDoesntExists() {
        //Arrange
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testUser);
        testUser.setEnabled(false);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsername(testUser.getUsername()));
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getByUsername(testUser.getUsername());
    }

    @Test
    public void getAll_ShouldReturn_AllUsers() {
        //Arrange
        Mockito.when(mockUserRepository.getAll()).thenReturn(userList);

        //Act
        Iterable<User> toCompare = userService.getAll();

        //Assert
        Assertions.assertEquals(toCompare, userList);
    }


    @Test
    public void updatePassword_ShouldUpdateUser_WhenInputIsValid() {

        ///Act
        userService.updatePassword(testUser);

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
    }

    @Test
    public void enableRegisteredUser_ShouldEnableUser_WhenTokenIsValid() {
        //Arrange
        Mockito.when(mockUserRepository.getVerificationToken(Mockito.anyString()))
                .thenReturn(testVerificationToken);
        long nowMilliSec = System.currentTimeMillis() + 1000;
        testVerificationToken.setExpiryDate(new Date(nowMilliSec));

        ///Act
        userService.enableRegisteredUser(testUser, testVerificationToken);

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
    }

    @Test
    public void enableRegisteredUser_ShouldThrow_WhenTokenIsExpired() {
        //Arrange
        Mockito.when(mockUserRepository.getVerificationToken(Mockito.anyString()))
                .thenReturn(testVerificationToken);
        long nowMilliSec = System.currentTimeMillis() - 1;
        testVerificationToken.setExpiryDate(new Date(nowMilliSec));

        //Act, Assert
        Assertions.assertThrows(ExpiredTokenException.class,
                () -> userService.enableRegisteredUser(testUser, testVerificationToken));
        Mockito.verify(mockUserRepository,
                Mockito.times(0)).update(testUser);
    }

    @Test
    public void getPasswordResetToken_ShouldGetToken_WhenTokenExist() {
        //Arrange
        Mockito.when(mockUserRepository.getPasswordResetToken(Mockito.anyString()))
                .thenReturn(testPasswordToken);

        //Act
        PasswordResetToken toCompare = userService.getPasswordResetToken(testUser.getUsername());

        //Assert
        Assertions.assertEquals(testPasswordToken.getToken(), toCompare.getToken());
    }

//    @Test
//    public void createPasswordResetToken_ShouldCreateToken() {
//        //Arrange
//        PasswordResetToken passwordResetToken = Mockito.mock(PasswordResetToken.class);
//
//        //Act
//        userService.createPasswordResetToken(testUser, testPasswordToken.getToken());
//
//        //Assert
//        Mockito.verify(mockUserRepository,
//                Mockito.times(1)).savePasswordResetToken(testPasswordToken);
//    }

    @Test
    public void existByNameActive_ShouldReturnTrue_WhenUserExist() {
        //Arrange
        Mockito.when(mockUserRepository.existsByNameActive(Mockito.anyString()))
                .thenReturn(true);

        ///Act
        userService.existByNameActive(testUser.getUsername());

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).existsByNameActive(testUser.getUsername());
    }

    @Test
    public void delete_ShouldDelete_WhenUserExistsAndPrincipleIsUser() {
        //Arrange
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("test@gmail.com");
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testUser);

        //Act
        userService.delete(testUser, mockPrincipal);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(testUser);
    }

    @Test
    public void delete_ShouldThrow_WhenUserNotAuthorised() {
        //Arrange
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("principle@gmail.com");
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testUser);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedException.class,
                () -> userService.delete(testUser, mockPrincipal));
        Mockito.verify(mockUserRepository, Mockito.times(0)).delete(testUser);
    }

    @Test
    public void updateEmail_ShouldUpdateEmail_WhenUserIsAdmin() {
        //Arrange
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("admin@gmail.com");
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(testAdmin);
        Mockito.doNothing().when(mockEmailService).emailUserForEmailUpdate(testUser);

        //Act
        userService.updateEmail(testUser, mockPrincipal.getName());

        //Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
        Mockito.verify(mockAuthorityRepository,
                Mockito.times(1)).updateUsername(testUser);
    }


}
