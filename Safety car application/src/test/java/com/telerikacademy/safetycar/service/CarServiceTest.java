package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.District;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.CarRepository;
import com.telerikacademy.safetycar.services.CarServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.createCar;
import static com.telerikacademy.safetycar.TestObjectsFactory.createUser;

@ExtendWith(MockitoExtension.class)
public class CarServiceTest {

    @InjectMocks
    private CarServiceImpl carService;

    @Mock
    private CarRepository mockCarRepository;

    private User testUser;
    private Car testCar;
    private List<Car> testCarList;

    @BeforeEach
    public void init() {
        testUser = createUser();
        testCar = createCar();
        testCarList = Collections.singletonList(testCar);
    }

    @Test
    public void create_ShouldCreateCar_WhenInputIsValid() {
        //Arrange
        Mockito.when(mockCarRepository.existsByRegNumber(Mockito.any()))
                .thenReturn(false);

        //Act
        carService.save(testUser, testCar);

        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).create(testCar);
    }

    @Test
    public void existsByRegNumber_ShouldReturnTrue_WhenCarExist() {
        //Arrange
        Mockito.when(mockCarRepository.existsByRegNumber(Mockito.any()))
                .thenReturn(false);

        //Act
        carService.existsByRegNumber(testCar.getRegistrationNumber());

        //Assert
        Mockito.verify(mockCarRepository,
                Mockito.times(1)).existsByRegNumber(testCar.getRegistrationNumber());
    }

    @Test
    public void getByRegistrationNumber_ShouldReturnCar_WhenCarExist() {
        //Arrange
        Mockito.when(mockCarRepository.getByRegistrationNumber(Mockito.anyString()))
                .thenReturn(testCar);

        //Act
        Car carToCompare = carService.getByRegistrationNumber(testCar.getRegistrationNumber());

        //Assert
        Assertions.assertEquals(carToCompare, testCar);
    }

    @Test
    public void getAllCarsOfUser_ShouldReturnListOfCars() {
        //Arrange
        Mockito.when(mockCarRepository.getAllCarsOfUser(testUser.getId()))
                .thenReturn(testCarList);

        //Act
        List<Car> listToCompare = carService.getAllCarsOfUser(testUser.getId());

        //Assert
        Assertions.assertEquals(testCarList, listToCompare);
    }

}
