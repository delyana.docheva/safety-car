package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.CarBrand;
import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.repositories.CarBrandRepository;
import com.telerikacademy.safetycar.repositories.CarModelRepository;
import com.telerikacademy.safetycar.services.CarBrandServiceImpl;
import com.telerikacademy.safetycar.services.CarModelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.createBrand;
import static com.telerikacademy.safetycar.TestObjectsFactory.createModel;

@ExtendWith(MockitoExtension.class)
public class CarBrandServiceTest {

    @InjectMocks
    private CarBrandServiceImpl carService;

    @Mock
    private CarBrandRepository mockCarRepository;

    private CarBrand testBrand;
    private List<CarBrand> testListBrand;

    @BeforeEach
    public void initiate() {
        testBrand = createBrand();
        testListBrand = Collections.singletonList(testBrand);
    }

    @Test
    public void getAll_ShouldReturnListOfModels() {
        //Arrange
        Mockito.when(mockCarRepository.getAll())
                .thenReturn(testListBrand);

        //Act
        List<CarBrand> listToCompare = carService.getAll();

        //Assert
        Assertions.assertEquals(testListBrand, listToCompare);
    }


}

