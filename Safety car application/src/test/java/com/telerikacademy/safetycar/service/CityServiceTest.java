package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.District;
import com.telerikacademy.safetycar.repositories.CityRepository;
import com.telerikacademy.safetycar.services.CityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.createCity;
import static com.telerikacademy.safetycar.TestObjectsFactory.createDistrict;

@ExtendWith(MockitoExtension.class)
public class CityServiceTest {
    @InjectMocks
    private CityServiceImpl cityService;

    @Mock
    private CityRepository mockCityRepository;

    private City testCity;
    private List<City> testListCities;

    @BeforeEach
    public void initiate() {
        testCity = createCity();
        testListCities = Collections.singletonList(testCity);
    }

    @Test
    public void getAll_ShouldReturnListOfCities() {
        //Arrange
        Mockito.when(mockCityRepository.getAll())
                .thenReturn(testListCities);

        //Act
        List<City> listToCompare = cityService.getAll();

        //Assert
        Assertions.assertEquals(testListCities, listToCompare);
    }

    @Test
    public void getByName_ShouldReturn_WhenCityExists() {
        //Arrange
        Mockito.when(mockCityRepository.getByName(Mockito.anyString()))
                .thenReturn(testCity);

        //Act
        City cityToCompare = cityService.getByName(testCity.getName());

        //Assert
        Assertions.assertEquals(testCity.getId(), cityToCompare.getId());
        Assertions.assertEquals(testCity.getName(), cityToCompare.getName());
        Assertions.assertEquals(testCity.getDistrict(), cityToCompare.getDistrict());
    }

    @Test
    public void getById_ShouldReturn_WhenCityExists() {
        //Arrange
        Mockito.when(mockCityRepository.getById(Mockito.anyInt()))
                .thenReturn(testCity);

        //Act
        City cityToCompare = cityService.getById(testCity.getId());

        //Assert
        Assertions.assertEquals(testCity.getId(), cityToCompare.getId());
        Assertions.assertEquals(testCity.getName(), cityToCompare.getName());
    }

    @Test
    public void filterByDistrictId_ShouldReturnListOfDistricts() {
        //Arrange
        Mockito.when(mockCityRepository.filterByDistrictId(testCity.getDistrict().getId()))
                .thenReturn(testListCities);

        //Act
        List<City> listToCompare = cityService.filterByDistrictId(testCity.getDistrict().getId());

        //Assert
        Assertions.assertEquals(testListCities, listToCompare);
    }
}
