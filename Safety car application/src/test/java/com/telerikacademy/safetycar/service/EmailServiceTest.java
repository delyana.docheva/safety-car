package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.verification.OnExpiredTokenEvent;
import com.telerikacademy.safetycar.models.verification.OnPasswordResetEvent;
import com.telerikacademy.safetycar.models.verification.OnRegistrationCompleteEvent;
import com.telerikacademy.safetycar.repositories.CarRepository;
import com.telerikacademy.safetycar.services.CarServiceImpl;
import com.telerikacademy.safetycar.services.EmailServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Locale;

import static com.telerikacademy.safetycar.TestObjectsFactory.createPolicy;
import static com.telerikacademy.safetycar.TestObjectsFactory.createUser;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {
    @InjectMocks
    private EmailServiceImpl emailService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private JavaMailSender mailSender;

    private InsurancePolicy policy;
    private User user;

    @BeforeEach
    void initiate() {
        policy = createPolicy();
        user = createUser();
    }


    @Test
    public void emailUserForAgentChangedStatus_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.emailUserForAgentChangedStatus(policy);
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }

    @Test
    public void emailUserForSoonExpiringPolicy_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.emailUserForSoonExpiringPolicy(policy);
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }

    @Test
    public void emailUserForExpiredPolicy_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.emailUserForExpiredPolicy(policy);
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }

    @Test
    public void generateOnRegistrationMail_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.generateOnRegistrationMail(new OnRegistrationCompleteEvent(user,
                new Locale("en"), "appUrl"), user, "token");
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }

    @Test
    public void generateNewVerificationTokenMail_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.generateNewVerificationTokenMail(new OnExpiredTokenEvent(user,
                new Locale("en"), "appUrl"), user, "token");
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }

    @Test
    public void generatePasswordResetMail_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.generatePasswordResetMail(new OnPasswordResetEvent(user,
                new Locale("en"), "appUrl"), user, "token");
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }

    @Test
    public void emailUserForEmailUpdate_ShouldSendMail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        mailSender.send(message);

        //Act
        emailService.emailUserForEmailUpdate(user);
        //Assert
        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }
}
