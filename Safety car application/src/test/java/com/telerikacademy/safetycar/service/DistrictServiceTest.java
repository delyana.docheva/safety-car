package com.telerikacademy.safetycar.service;

import com.telerikacademy.safetycar.models.CarBrand;
import com.telerikacademy.safetycar.models.District;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.CarBrandRepository;
import com.telerikacademy.safetycar.repositories.DistrictRepository;
import com.telerikacademy.safetycar.services.CarBrandServiceImpl;
import com.telerikacademy.safetycar.services.DistrictService;
import com.telerikacademy.safetycar.services.DistrictServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.TestObjectsFactory.createDistrict;

@ExtendWith(MockitoExtension.class)
public class DistrictServiceTest {
    @InjectMocks
    private DistrictServiceImpl districtService;

    @Mock
    private DistrictRepository mockDistrictRepository;

    private District testDistrict;
    private List<District> testListDistricts;

    @BeforeEach
    public void initiate() {
        testDistrict = createDistrict();
        testListDistricts = Collections.singletonList(testDistrict);
    }

    @Test
    public void getAll_ShouldReturnListOfDistricts() {
        //Arrange
        Mockito.when(mockDistrictRepository.getAll())
                .thenReturn(testListDistricts);

        //Act
        List<District> listToCompare = districtService.getAll();

        //Assert
        Assertions.assertEquals(testListDistricts, listToCompare);
    }

    @Test
    public void getByName_ShouldReturn_WhenDistrictExists() {
        //Arrange
        Mockito.when(mockDistrictRepository.getByName(Mockito.anyString()))
                .thenReturn(testDistrict);

        //Act
        District districtToCompare = districtService.getByName(testDistrict.getName());

        //Assert
        Assertions.assertEquals(testDistrict.getId(), districtToCompare.getId());
        Assertions.assertEquals(testDistrict.getName(), districtToCompare.getName());
        Assertions.assertEquals(testDistrict.getCountry(), districtToCompare.getCountry());
    }

    @Test
    public void getById_ShouldReturn_WhenDistrictExists() {
        //Arrange
        Mockito.when(mockDistrictRepository.getById(Mockito.anyInt()))
                .thenReturn(testDistrict);

        //Act
        District districtToCompare = districtService.getById(testDistrict.getId());

        //Assert
        Assertions.assertEquals(testDistrict.getId(), districtToCompare.getId());
        Assertions.assertEquals(testDistrict.getName(), districtToCompare.getName());
    }

    @Test
    public void filterByCountryId_ShouldReturnListOfDistricts() {
        //Arrange
        Mockito.when(mockDistrictRepository.filterByCountryId(testDistrict.getCountry().getId()))
                .thenReturn(testListDistricts);

        //Act
        List<District> listToCompare = districtService.filterByCountryId(testDistrict.getCountry().getId());

        //Assert
        Assertions.assertEquals(testListDistricts, listToCompare);
    }
}
