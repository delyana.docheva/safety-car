package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.CarBrand;
import com.telerikacademy.safetycar.repositories.CarBrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarBrandServiceImpl implements CarBrandService {
    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrand> getAll() {
        return carBrandRepository.getAll();
    }
}
