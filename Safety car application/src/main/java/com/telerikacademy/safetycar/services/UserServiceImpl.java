package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.*;
import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.AuthorityType;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.models.verification.VerificationToken;
import com.telerikacademy.safetycar.repositories.AuthorityRepository;
import com.telerikacademy.safetycar.repositories.InsurancePolicyRepository;
import com.telerikacademy.safetycar.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    public static final String EXPIRED_VERIFICATION_TOKEN_MESSAGE = "Your verification token has expired, please request new one.";
    public static final String USER_BY_ID_NOT_ACTIVE = "User with id %d does not exist or is not active.";
    public static final String USER_BY_NAME_NOT_ACTIVE = "User with username %s does not exist or is not active.";
    public static final String USER_NOT_AUTHORISED = "User %s is not authorised for that update.";
    public static final String DUPLICATE_USERNAME_MESSAGE = "There already is an account created with %s email.";
    public static final String ACTIVE_REQUESTS_MESSAGE = "You have active policy requests and currently cannot update your information.";
    public static final String PASSWORD_UPDATE_NOT_ALLOWED = "Password update is only allowed to account owner.";

    private final UserRepository userRepository;
    private final InsurancePolicyRepository policyRepository;
    private final AuthorityRepository authorityRepository;
    private final EmailService emailService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, InsurancePolicyRepository policyRepository,
                           AuthorityRepository authorityRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.policyRepository = policyRepository;
        this.authorityRepository = authorityRepository;
        this.emailService = emailService;
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User userDetailsCustom = userRepository.getByUsername(username);

        return buildUserFromUserEntity(userDetailsCustom);
    }

    private org.springframework.security.core.userdetails.User buildUserFromUserEntity(User userEntity) {
        // convert model user to spring security user
        String username = userEntity.getUsername();
        String password = userEntity.getPassword();
        boolean enabled = userEntity.isEnabled();
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        List<Authority> authoritiesList = new ArrayList<>(userEntity.getAuthorities());
        GrantedAuthority[] authorities = new SimpleGrantedAuthority[authoritiesList.size()];
        for (int i = 0; i < authoritiesList.size(); i++) {
            authorities[i] = new SimpleGrantedAuthority(authoritiesList.get(i).toString());
        }

        org.springframework.security.core.userdetails.User springUser =
                new org.springframework.security.core.userdetails.User(username,
                        password,
                        enabled,
                        accountNonExpired,
                        credentialsNonExpired,
                        accountNonLocked,
                        Arrays.asList(authorities));
        return springUser;
    }

    @Override
    public List<User> search(String username, String phone, int orderBy) {
        return userRepository.search(username, phone, orderBy);
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken tokenEntity = new VerificationToken(token, user);
        userRepository.saveVerificationToken(tokenEntity);
    }

    @Override
    public void enableRegisteredUser(User user, VerificationToken verificationToken) {
        Date tokenExpiryDate = userRepository.getVerificationToken(verificationToken.getToken()).getExpiryDate();
        Date now = new Date();
        if (tokenExpiryDate.before(now)) {
            throw new ExpiredTokenException(EXPIRED_VERIFICATION_TOKEN_MESSAGE);
        }
        userRepository.update(user);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return userRepository.getVerificationToken(token);
    }

    @Override
    public void createPasswordResetToken(User user, String token) {
        PasswordResetToken tokenEntity = new PasswordResetToken(token, user);
        userRepository.savePasswordResetToken(tokenEntity);
    }

    @Override
    public PasswordResetToken getPasswordResetToken(String token) {
        return userRepository.getPasswordResetToken(token);
    }

    @Override
    public void updatePassword(User user) {
        userRepository.update(user);
    }

    @Override
    public void create(User user) {
        validateUniqueUsername(user.getUsername());
        userRepository.create(user);

        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(user.getId(), user.getUsername(), AuthorityType.ROLE_USER.name()));
        user.setAuthorities(authorities);
        userRepository.update(user);
    }

    @Override
    public User getActiveById(int id) {
        User result = userRepository.getById(id);
        if (result.isDeleted() || !result.isEnabled()) {
            throw new EntityNotFoundException(String.format(USER_BY_ID_NOT_ACTIVE, id));
        }
        return result;
    }

    @Override
    public User getById(int id) {
        User user = userRepository.getById(id);
        if (!user.isEnabled() && user.isDeleted()) {
            throw new EntityNotFoundException(String.format(USER_BY_ID_NOT_ACTIVE, id));
        }
        return user;
    }

    @Override
    public User getByUsername(String username) {
        User result = userRepository.getByUsername(username);
        if (result.isDeleted() || !result.isEnabled()) {
            throw new EntityNotFoundException(String.format(USER_BY_NAME_NOT_ACTIVE, username));
        }
        return result;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public boolean existByNameActive(String username) {
        return userRepository.existsByNameActive(username);
    }

    @Override
    public void update(User user, String principalName) {
        User loggedUser = userRepository.getByUsername(principalName);
        if (checkIfUserHasPolicyWithStatus(user, PolicyStatusType.REQUESTED) && isNotAdmin(loggedUser)) {
            throw new InvalidOperationException(ACTIVE_REQUESTS_MESSAGE);
        }
        if (!user.getUsername().equals(principalName) && isNotAdmin(loggedUser)) {
            throw new UnauthorizedException(String.format(USER_NOT_AUTHORISED,
                    user.getUsername()));
        }
        if (user.getHasAccident() && checkIfUserHasPolicyWithStatus(user, PolicyStatusType.PENDING) && isNotAdmin(loggedUser)) {
            throw new UnauthorizedException(String.format(USER_NOT_AUTHORISED,
                    user.getUsername()));
        }
        if (userRepository.existsByName(user.getUsername()) &&
                userRepository.getByUsername(user.getUsername()).getId() != user.getId()) {
            throw new DuplicateEntityException(String.format(DUPLICATE_USERNAME_MESSAGE, user.getUsername()));
        }

        userRepository.update(user);
    }

    @Override
    public void updatePassword(User user, Principal principal) {
        if (!user.getUsername().equals(principal.getName())) {
            throw new InvalidOperationException(PASSWORD_UPDATE_NOT_ALLOWED);
        }
        userRepository.update(user);
    }

    @Override
    public void updateEmail(User user, String principalName) {
        User loggedUser = userRepository.getByUsername(principalName);
        if (isNotAdmin(loggedUser)) {
            throw new UnauthorizedException(String.format(USER_NOT_AUTHORISED,
                    loggedUser.getUsername()));
        }
        userRepository.update(user);
        authorityRepository.updateUsername(user);
        emailService.emailUserForEmailUpdate(user);

    }

    @Override
    public void delete(User user, Principal principal) {
        User loggedUser = userRepository.getByUsername(principal.getName());
        if (!user.getUsername().equals(principal.getName()) && isNotAdmin(loggedUser)) {
            throw new UnauthorizedException(String.format(USER_NOT_AUTHORISED,
                    user.getUsername()));
        }
        user.setEnabled(false);
        user.setDeleted(true);
        userRepository.delete(user);
    }

    private void validateUniqueUsername(String name) {
        if (userRepository.existsByName(name)) {
            throw new DuplicateEntityException(
                    String.format(DUPLICATE_USERNAME_MESSAGE, name));
        }
    }

    private boolean isNotAdmin(User loggedUser) {
        return loggedUser.getAuthorities().stream()
                .noneMatch(authority -> authority.getAuthority().equals(AuthorityType.ROLE_ADMIN.name()));
    }

    private boolean checkIfUserHasPolicyWithStatus(User user, PolicyStatusType typeToSearch) {
        return policyRepository.getPoliciesForUser(user.getId()).stream()
                .anyMatch(status -> status.getStatus().getType().equals(typeToSearch));
    }

}