package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService, IUserService {

    void create(User item);

    void update(User itemToUpdate, String principalName);

    void delete(User user, Principal principal);

    void updatePassword(User user, Principal principal);

    User getActiveById(int id);

    User getById(int id);

    User getByUsername(String username);

    List<User> getAll();

    List<User> search(String username, String phone, int orderBy);

    boolean existByNameActive(String username);

    void updateEmail(User user, String name);
}
