package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService{

    private final CityRepository repository;

    @Autowired
    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<City> getAll() {
        return repository.getAll();
    }

    @Override
    public City getByName(String city) {
            return repository.getByName(city);
    }

    @Override
    public List<City> filterByDistrictId(int districtId) {
        return repository.filterByDistrictId(districtId);
    }

    @Override
    public City getById(int id) {
        return repository.getById(id);
    }

}
