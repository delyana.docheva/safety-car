package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.City;

import java.util.List;

public interface CityService {
    City getById(int id);

    List<City> getAll();

    City getByName(String city);

    List<City> filterByDistrictId(int districtId);
}
