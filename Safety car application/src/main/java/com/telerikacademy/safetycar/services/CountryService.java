package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();
}
