package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.PolicyStatus;

import java.util.List;

public interface PolicyStatusService {
    List<PolicyStatus> getAll();
}
