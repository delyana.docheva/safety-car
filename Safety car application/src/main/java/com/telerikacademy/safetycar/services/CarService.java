package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.User;

import java.util.List;

public interface CarService {
    void save(User owner, Car car);

    Car getByRegistrationNumber(String registrationNumber);

    boolean existsByRegNumber(String registrationNumber);

    List<Car> getAllCarsOfUser(int userId);
}
