package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.BadRequestException;
import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.CarRepository;
import com.telerikacademy.safetycar.repositories.InsurancePolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {
    public static final String CAR_HAS_ACTIVE_REQUESTS = "Car with registration number %s has active policy requests and can not be update.";

    private final CarRepository carRepository;
    private final InsurancePolicyRepository policyRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, InsurancePolicyRepository policyRepository) {
        this.carRepository = carRepository;
        this.policyRepository = policyRepository;
    }

    @Override
    public void save(User owner, Car car) {
        if (carRepository.existsByRegNumber(car.getRegistrationNumber())) {
            if (carHasRequestedPolicy(car)){
                throw new BadRequestException(String.format(CAR_HAS_ACTIVE_REQUESTS, car.getRegistrationNumber()));
            }
            carRepository.update(car);
        } else {
            car.setOwner(owner);
            carRepository.create(car);
        }
    }

    @Override
    public Car getByRegistrationNumber(String registrationNumber) {
        return carRepository.getByRegistrationNumber(registrationNumber);
    }

    @Override
    public boolean existsByRegNumber(String registrationNumber) {
        return carRepository.existsByRegNumber(registrationNumber);
    }

    @Override
    public List<Car> getAllCarsOfUser(int userId) {
        return carRepository.getAllCarsOfUser(userId);
    }

    private boolean carHasRequestedPolicy(Car car) {
        return policyRepository.getAllRequestedPolicies().stream()
                .anyMatch(policy -> policy.getCar().getId() == car.getId());
    }

}
