package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.models.verification.VerificationToken;

public interface IUserService {
    void createVerificationToken(User user, String token);

    void enableRegisteredUser(User user, VerificationToken verificationToken);

    VerificationToken getVerificationToken(String token);

    void createPasswordResetToken(User user, String token);

    PasswordResetToken getPasswordResetToken(String token);

    void updatePassword(User user);
}
