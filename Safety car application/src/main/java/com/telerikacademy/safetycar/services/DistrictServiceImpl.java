package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.District;
import com.telerikacademy.safetycar.repositories.CityRepository;
import com.telerikacademy.safetycar.repositories.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistrictServiceImpl implements DistrictService{
    private final DistrictRepository repository;

    @Autowired
    public DistrictServiceImpl(DistrictRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<District> getAll() {
        return repository.getAll();
    }

    @Override
    public District getByName(String city) {
        return repository.getByName(city);
    }

    @Override
    public List<District> filterByCountryId(int countryId) {
        return repository.filterByCountryId(countryId);
    }

    @Override
    public District getById(int id) {
        return repository.getById(id);
    }
}
