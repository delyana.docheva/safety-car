package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.verification.OnExpiredTokenEvent;
import com.telerikacademy.safetycar.models.verification.OnPasswordResetEvent;
import com.telerikacademy.safetycar.models.verification.OnRegistrationCompleteEvent;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);

    void generateOnRegistrationMail(OnRegistrationCompleteEvent event, User user, String token);

    void generateNewVerificationTokenMail(OnExpiredTokenEvent event, User user, String token);

    void generatePasswordResetMail(OnPasswordResetEvent event, User user, String token);

    void emailUserForPolicyActivation(InsurancePolicy policy);

    void emailUserForAgentChangedStatus(InsurancePolicy policy);

    void emailUserForSoonExpiringPolicy(InsurancePolicy policy);

    void emailUserForExpiredPolicy(InsurancePolicy policy);

    void emailUserForEmailUpdate(User user);
}
