package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.repositories.PolicyStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PolicyStatusServiceImpl implements PolicyStatusService {

    private final PolicyStatusRepository policyStatusRepository;

    @Autowired
    public PolicyStatusServiceImpl(PolicyStatusRepository policyStatusRepository) {
        this.policyStatusRepository = policyStatusRepository;
    }

    @Override
    public List<PolicyStatus> getAll() {
        return policyStatusRepository.getAll();
    }
}
