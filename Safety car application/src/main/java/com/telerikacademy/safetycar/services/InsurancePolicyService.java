package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface InsurancePolicyService {

    InsurancePolicy getById(int id);

    List<InsurancePolicy> getAll();

    List<InsurancePolicy> getPoliciesForUser(String username);

    void requestPolicy(InsurancePolicy policy, String loggedUsername);

    void update(InsurancePolicy policy, String loggedUsername);

    void delete(int policyId, String loggedUsername);

    double calculateExpectedPremium(InsurancePolicy toCalculate);

    int save(InsurancePolicy toSave, User owner);

    List<InsurancePolicy> getAllRequestedPolicies();

    int calculateAge(LocalDate startDate);

    void changePolicyStatus(PolicyStatusType pending, InsurancePolicy policy, String loggedUsername);

    List<InsurancePolicy> search(String userEmail, String firstName, String lastName, int policyId, int premiumMin, int premiumMax, int statusId
            , Date requestedDateFrom, Date requestedDateTo, LocalDate startDateFrom, LocalDate startDateTo);

    void checkIfLoggedUserIsAllowedToReadPolicy(String ownerUsername, String loggedUsername);

    boolean requestedPoliciesByCarRegNumber(String carRegistrationNumber);

    boolean validateAccidentsInput();
}
