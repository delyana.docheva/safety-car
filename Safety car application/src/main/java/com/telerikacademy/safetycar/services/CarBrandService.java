package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandService {
    List<CarBrand> getAll();;
}
