package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.PaymentStatusType;
import com.telerikacademy.safetycar.models.verification.OnExpiredTokenEvent;
import com.telerikacademy.safetycar.models.verification.OnPasswordResetEvent;
import com.telerikacademy.safetycar.models.verification.OnRegistrationCompleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;

@Service
public class EmailServiceImpl implements EmailService {
    public static final String EXPIRING_POLICY_SUBJECT = "Safety Car - expiring policy reminder";
    public static final String ACTIVATED_POLICY_SUBJECT = "Safety Car - insurance policy activation";
    public static final String POLICY_STATUS_UPDATE_SUBJECT = "Your insurance policy's status has changed";
    public static final String ON_REGISTRATION_SUBJECT = "Registration Confirmation";
    public static final String FOOTER = "\nBest regards,\nSafety Car Team\n";
    public static final String RESET_PASSWORD_SUBJECT = "Reset Password Confirmation";
    public static final String RESET_EMAIL_SUBJECT = "Request for reset of email";

    private final JavaMailSender emailSender;

    @Autowired
    public EmailServiceImpl(@Qualifier("getJavaMailSender") JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycarwebapplication@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);

    }

    public void generateOnRegistrationMail(OnRegistrationCompleteEvent event, User user, String token) {
        String recipientAddress = user.getUsername();
        String content = String.format("Dear %s, thank you for joining out site! Please click the link below " +
                "to finish your registration.\n", user.getUsername());
        String confirmationUrl = event.getAppUrl() + "/confirm-registration?token=" + token;
        String message = content + "\r\n" + "http://localhost:8080" + confirmationUrl + "\r\n" + FOOTER;

        sendSimpleMessage(recipientAddress, ON_REGISTRATION_SUBJECT, message);
    }

    public void generateNewVerificationTokenMail(OnExpiredTokenEvent event, User user, String token) {
        String recipientAddress = user.getUsername();
        String content = String.format("Dear %s, the token for your registration has expired. Please click on the link below " +
                "to finish your registration.\n", user.getUsername());
        String confirmationUrl = event.getAppUrl() + "/confirm-registration?token=" + token;
        String message = content + "\r\n" + "http://localhost:8080" + confirmationUrl + "\r\n" + FOOTER;

        sendSimpleMessage(recipientAddress, ON_REGISTRATION_SUBJECT, message);

    }

    public void generatePasswordResetMail(OnPasswordResetEvent event, User user, String token) {
        String recipientAddress = user.getUsername();
        String content = String.format("Dear %s,\nWe have received a request on your behave for password reset. " +
                "If it was made by you please click on the link below to reset your password.\n", user.getUsername());
        String confirmationUrl = event.getAppUrl() + "/confirm-password?token=" + token;

        String message = content + "\r\n" + "http://localhost:8080" + confirmationUrl + "\r\n" + FOOTER;

        sendSimpleMessage(recipientAddress, RESET_PASSWORD_SUBJECT, message);
    }

    @Override
    public void emailUserForEmailUpdate(User user) {
        String recipientAddress = user.getUsername();
        String content = String.format("Dear %s,\nThe request for your email update has been processed. " +
                "Please use this email on your next login to out site.\n", user.getUsername());

        String message = content + "\r\n" + FOOTER;

        sendSimpleMessage(recipientAddress, RESET_EMAIL_SUBJECT, message);
    }

    public void emailUserForAgentChangedStatus(InsurancePolicy policy) {
        String recipientAddress = policy.getOwner().getUsername();
        String carBrandName = policy.getCar().getModel().getBrand().getName();
        String carModelName = policy.getCar().getModel().getName();
        String updatedStatus = policy.getStatus().getType().toString().toLowerCase();

        String message = String.format("Dear %s,\n \nWe would like to inform you that your policy for vehicle " +
                        "%s %s has been marked as %s.\n \nBest regards,\nSafety Car Team\n",
                recipientAddress, carBrandName, carModelName, updatedStatus);

        sendSimpleMessage(recipientAddress, POLICY_STATUS_UPDATE_SUBJECT, message);
    }

    public void emailUserForPolicyActivation(InsurancePolicy policy) {
        String recipientAddress = policy.getOwner().getUsername();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String stringActiveDate = policy.getStartDate().format(formatter);
        String stringExpDate = policy.getExpiringDate().minusDays(1).format(formatter);

        String message;

        if (policy.getPaymentStatus().getType().equals(PaymentStatusType.COMPLETE) ||
                policy.getPaymentStatus().getType().equals(PaymentStatusType.ON_TIME)) {
            message = String.format("Dear %s,\n\nWe would like to inform you that your policy #%d of vehicle %s " +
                            "%s is activated from today %s until %s. Drive safe!\n" + FOOTER,
                    policy.getOwner().getUsername(), policy.getId(),
                    policy.getCar().getModel().getBrand().getName(),
                    policy.getCar().getModel().getName(), stringActiveDate, stringExpDate);

        } else {
            message = String.format("Dear %s,\n\nYou have requested a policy #%d of vehicle %s %s with starting " +
                            "date %s. To activate it, you need to complete the payment transaction. Please " +
                            "contact your agent if further information is needed.\n" + FOOTER,
                    policy.getOwner().getUsername(), policy.getId(),
                    policy.getCar().getModel().getBrand().getName(),
                    policy.getCar().getModel().getName(), stringActiveDate);

        }
        sendSimpleMessage(recipientAddress, ACTIVATED_POLICY_SUBJECT, message);
    }

    public void emailUserForSoonExpiringPolicy(InsurancePolicy policy) {
        String recipientAddress = policy.getOwner().getUsername();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String stringExpDate = policy.getExpiringDate().format(formatter);
        String message = String.format("Dear %s,\n\nWe would like to inform you that your policy of vehicle %s %s" +
                        " is expiring on %s." +
                        "Please contact your agent for more information.\n" + FOOTER, policy.getOwner().getUsername(),
                policy.getCar().getModel().getBrand().getName(), policy.getCar().getModel().getName(),
                stringExpDate);

        sendSimpleMessage(recipientAddress, EXPIRING_POLICY_SUBJECT, message);
    }

    public void emailUserForExpiredPolicy(InsurancePolicy policy) {
        String recipientAddress = policy.getOwner().getUsername();
        String message = String.format("Dear %s,\n\nWe would like to inform you that your policy #%d of vehicle " +
                        "%s %s has expired." +
                        " Please contact your agent for more information.\n" + FOOTER, policy.getOwner().getUsername(),
                policy.getId(),
                policy.getCar().getModel().getBrand().getName(), policy.getCar().getModel().getName());

        sendSimpleMessage(recipientAddress, EXPIRING_POLICY_SUBJECT, message);
    }
}
