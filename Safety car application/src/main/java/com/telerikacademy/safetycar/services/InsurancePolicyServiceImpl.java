package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.*;
import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.*;
import com.telerikacademy.safetycar.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

@Service
public class InsurancePolicyServiceImpl implements InsurancePolicyService {

    public static final String USER_NOT_AUTHORISED = "User %s is not authorised for that update.";
    public static final String NOT_FOUND = "Insurance policy with id %d not found";
    public static final String ALREADY_REQUESTED = "Insurance policy with id %d has already been requested";
    public static final String FUTURE_DATE_NOT_ALLOWED = "Registration/ birth date cannot be in the future!";
    public static final String POLICY_NOT_REQUESTED = "You can only cancel policies with status requested";
    public static final String ACCIDENTS_INVALID_INFORMATION = "The provided information about accident history is " +
            "inaccurate. Please provide valid information.";

    private final InsurancePolicyRepository insurancePolicyRepository;
    private final PremiumCalculationRepository premiumCalculationRepository;
    private final PolicyStatusRepository policyStatusRepository;
    private final CarService carService;
    private final UserService userService;
    private final PaymentStatusRepository paymentStatusRepository;
    private final EmailService emailService;
    private Random random;

    @Autowired
    public InsurancePolicyServiceImpl(InsurancePolicyRepository insurancePolicyRepository,
                                      PremiumCalculationRepository premiumCalculationRepository,
                                      PolicyStatusRepository policyStatusRepository,
                                      CarService carService, UserService userService,
                                      PaymentStatusRepository paymentStatusRepository, EmailService emailService) {
        this.insurancePolicyRepository = insurancePolicyRepository;
        this.premiumCalculationRepository = premiumCalculationRepository;
        this.policyStatusRepository = policyStatusRepository;
        this.carService = carService;
        this.userService = userService;
        this.paymentStatusRepository = paymentStatusRepository;
        this.emailService = emailService;
        this.random = new Random();

    }

    @Override
    public InsurancePolicy getById(int id) {
        Optional<InsurancePolicy> result = insurancePolicyRepository.getById(id);
        verifyPolicyExists(result, id);
        return result.get();
    }

    @Override
    public List<InsurancePolicy> getAll() {
        return insurancePolicyRepository.getAll();
    }

    @Override
    public List<InsurancePolicy> getPoliciesForUser(String username) {
        User user = userService.getByUsername(username);
        return insurancePolicyRepository.getPoliciesForUser(user.getId());
    }

    @Override
    public void requestPolicy(InsurancePolicy policy, String loggedUsername) {
        checkIfCanBeRequested(policy.getId());
        policy.setStatus(policyStatusRepository.getByType(PolicyStatusType.REQUESTED));
        policy.setRequestedDate(new Date());
        policy.setPaymentStatus(paymentStatusRepository.getByType(PaymentStatusType.PENDING));
        policy.setExpiringDate(policy.getStartDate().plusMonths(12));

        if (policy.getPaymentMethod().getType().equals(PaymentMethodType.EXTENDED)) {
            double paymentIncreasing =
                    premiumCalculationRepository.getCoefficient(CoefficientType.PAYMENT_METHOD_COEFF);
            policy.setPremiumAmount(policy.getPremiumAmount() * paymentIncreasing);
        }
        update(policy, loggedUsername);
    }

    @Override
    public void update(InsurancePolicy policy, String loggedUsername) {
        User loggedUser = userService.getByUsername(loggedUsername);
        if (ownerAndLoggedAreDifferent(policy.getOwner().getUsername(), loggedUsername) && isNotAgent(loggedUser)
                && isNotAdmin(loggedUser)) {
            throw new AccessDeniedException(String.format(USER_NOT_AUTHORISED, loggedUsername));
        }
        insurancePolicyRepository.update(policy);
    }

    @Override
    public void delete(int policyId, String loggedUsername) {
        InsurancePolicy toDelete = getById(policyId);
        if (ownerAndLoggedAreDifferent(toDelete.getOwner().getUsername(), loggedUsername)) {
            throw new AccessDeniedException(String.format(USER_NOT_AUTHORISED, loggedUsername));
        }
        insurancePolicyRepository.delete(toDelete);
    }

    @Override
    public double calculateExpectedPremium(InsurancePolicy toCalculate) {
        double netPremium = calculateNetPremium(toCalculate.getCar(), toCalculate.getOwner());
        double tax = premiumCalculationRepository.getCoefficient(CoefficientType.TAX_COEFF);
        return netPremium * tax;
    }

    @Override
    public int save(InsurancePolicy policy, User owner) {
        userService.getActiveById(owner.getId());
        policy.setOwner(owner);
        policy.setSavedDate(new Date());
        policy.setStatus(policyStatusRepository.getByType(PolicyStatusType.GENERATED));

        Car car = policy.getCar();
        carService.save(owner, car);
        policy.setCar(carService.getByRegistrationNumber(car.getRegistrationNumber()));

        return insurancePolicyRepository.save(policy);
    }

    @Override
    public List<InsurancePolicy> getAllRequestedPolicies() {
        return insurancePolicyRepository.getAllRequestedPolicies();
    }

    public int calculateAge(LocalDate startDate) {
        LocalDate now = LocalDate.now();
        if (now.compareTo(startDate) < 0) {
            throw new InvalidOperationException(FUTURE_DATE_NOT_ALLOWED);
        }
        Period period = Period.between(startDate, now);
        return Math.abs(period.getYears());
    }

    @Override
    public void changePolicyStatus(PolicyStatusType statusType, InsurancePolicy policy, String loggedUsername) {
        User loggedUser = userService.getByUsername(loggedUsername);
        if (statusType.equals(PolicyStatusType.CANCELED)) {
            cancelPolicy(loggedUsername, policy);
        } else {
            if (isNotAgent(loggedUser) && isNotAdmin(loggedUser)) {
                throw new AccessDeniedException(String.format(USER_NOT_AUTHORISED, loggedUsername));
            }
            policy.setStatus(policyStatusRepository.getByType(statusType));
            policy.setAgent(loggedUser);
            update(policy, policy.getOwner().getUsername());
            emailService.emailUserForAgentChangedStatus(policy);
        }
    }

    @Override
    public List<InsurancePolicy> search(String userEmail, String firstName, String lastName, int policyId,
                                        int premiumMin, int premiumMax, int statusId, Date requestedDateFrom,
                                        Date requestedDateTo, LocalDate startDateFrom, LocalDate startDateTo) {

        return insurancePolicyRepository.search(userEmail, firstName, lastName, policyId, premiumMin, premiumMax,
                statusId, requestedDateFrom, requestedDateTo, startDateFrom, startDateTo);
    }

    @Override
    public void checkIfLoggedUserIsAllowedToReadPolicy(String ownerUsername, String loggedUsername) {
        User loggedUser = userService.getByUsername(loggedUsername);
        if (ownerAndLoggedAreDifferent(ownerUsername, loggedUsername) && isNotAgent(loggedUser)) {
            throw new AccessDeniedException(String.format(USER_NOT_AUTHORISED, loggedUsername));
        }
    }

    @Override
    public boolean requestedPoliciesByCarRegNumber(String carRegistrationNumber) {
        List<InsurancePolicy> activeRequests = insurancePolicyRepository.getAllRequestedPolicies();
        return checkIfCarIsPresent(carRegistrationNumber, activeRequests);
    }

    @Override
    public boolean validateAccidentsInput() {
        return accidentsRandomVerification();
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void scheduleCheckOfExpiringPolicies() {
        List<InsurancePolicy> activePolicies = insurancePolicyRepository.getAllActive();
        LocalDate today = LocalDate.now();
        LocalDate dateToCompare = today.plusDays(4);

        for (InsurancePolicy policy : activePolicies) {
            if (policy.getExpiringDate().isAfter(today) && policy.getExpiringDate().isBefore(dateToCompare)) {
                emailService.emailUserForSoonExpiringPolicy(policy);
            }
        }
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void scheduleStatusUpdate() {
        List<InsurancePolicy> allPolicies = insurancePolicyRepository.getAll();

        LocalDate today = LocalDate.now();

        for (InsurancePolicy policy : allPolicies) {
            if (policy.getExpiringDate().isEqual(today)) {
                policy.setStatus(policyStatusRepository.getByType(PolicyStatusType.EXPIRED));
                insurancePolicyRepository.update(policy);
                emailService.emailUserForExpiredPolicy(policy);
                continue;
            }
            if (policy.getStartDate().isEqual(today) && policy.getStatus().getType().equals(PolicyStatusType.APPROVED)) {
                if (policy.getPaymentStatus().getType().equals(PaymentStatusType.COMPLETE) ||
                        policy.getPaymentStatus().getType().equals(PaymentStatusType.ON_TIME)) {
                    policy.setStatus(policyStatusRepository.getByType(PolicyStatusType.ACTIVE));
                } else {
                    policy.setStartDate(today.plusDays(1));
                }
                insurancePolicyRepository.update(policy);
                emailService.emailUserForPolicyActivation(policy);
            }
        }

    }

    private void verifyPolicyExists(Optional<InsurancePolicy> policy, int id) {
        if (policy.isEmpty()) {
            throw new EntityNotFoundException(String.format(NOT_FOUND, id));
        }
    }

    private void checkIfCanBeRequested(int id) {
        if (getById(id).getStatus().getType() != PolicyStatusType.GENERATED) {
            throw new InvalidOperationException(String.format(ALREADY_REQUESTED, id));
        }

    }

    private boolean accidentsRandomVerification() {
        return random.nextBoolean();
    }

    private boolean ownerAndLoggedAreDifferent(String ownerUsername, String loggedUsername) {
        User logged = userService.getByUsername(loggedUsername);
        return !ownerUsername.equals(loggedUsername);
    }

    private double calculateNetPremium(Car car, User carOwner) {
        int carAge = calculateAge(car.getRegistrationDate());
        double baseAmount = premiumCalculationRepository.getBaseAmount(car.getCubicCapacity(), carAge);
        int driverAge = calculateAge(carOwner.getBirthDate());
        double driverAgeCoeff = 1;
        if (driverAge < premiumCalculationRepository.getCoefficient(CoefficientType.MIN_DRIVER_AGE_FOR_DISCOUNT)) {
            driverAgeCoeff = premiumCalculationRepository.getCoefficient(CoefficientType.DRIVER_AGE_COEFF);
        }
        double accidentCoeff = 1;
        if (carOwner.getHasAccident()) {
            accidentCoeff = premiumCalculationRepository.getCoefficient(CoefficientType.ACCIDENT_COEFF);
        }
        return baseAmount * accidentCoeff * driverAgeCoeff;
    }

    private boolean isNotAgent(User loggedUser) {
        return loggedUser.getAuthorities().stream()
                .noneMatch(authority -> authority.getAuthority().equals("ROLE_AGENT"));
    }

    private boolean isNotAdmin(User loggedUser) {
        return loggedUser.getAuthorities().stream()
                .noneMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"));
    }

    private boolean checkIfCarIsPresent(String carRegistrationNumber, List<InsurancePolicy> activeRequests) {
        return activeRequests.stream()
                .anyMatch(policy -> policy.getCar().getRegistrationNumber().equals(carRegistrationNumber));
    }

    private void cancelPolicy(String loggedUsername, InsurancePolicy policy) {
        if (policy.getStatus().getType().equals(PolicyStatusType.REQUESTED) ||
                policy.getStatus().getType().equals(PolicyStatusType.GENERATED)) {
            policy.setStatus(policyStatusRepository.getByType(PolicyStatusType.CANCELED));
            update(policy, loggedUsername);
        } else {
            throw new InvalidOperationException(POLICY_NOT_REQUESTED);
        }
    }
}
