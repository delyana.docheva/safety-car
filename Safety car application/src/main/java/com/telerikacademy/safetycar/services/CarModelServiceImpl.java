package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.repositories.CarModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarModelServiceImpl implements CarModelService {
    private final CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }

    @Override
    public CarModel getById(int modelId) {
        return carModelRepository.getById(modelId);
    }

    public List<CarModel> getByBrandId(int brandId) {
        return carModelRepository.getByBrandId(brandId);
    }
}
