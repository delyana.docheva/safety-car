package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.District;

import java.util.List;

public interface DistrictService {
    District getById(int id);

    List<District> getAll();

    District getByName(String name);

    List<District> filterByCountryId(int countryId);
}
