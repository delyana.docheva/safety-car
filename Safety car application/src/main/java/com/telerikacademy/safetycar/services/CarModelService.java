package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.CarModel;

import java.util.List;

public interface CarModelService {
    CarModel getById(int modelId);

    List<CarModel> getByBrandId(int brandId);
}
