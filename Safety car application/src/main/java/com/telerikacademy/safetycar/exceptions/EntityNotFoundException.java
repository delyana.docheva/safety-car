package com.telerikacademy.safetycar.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(){

    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
