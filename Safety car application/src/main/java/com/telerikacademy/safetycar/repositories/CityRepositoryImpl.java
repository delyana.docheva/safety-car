package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityRepositoryImpl implements CityRepository{

    SessionFactory factory;

    @Autowired
    public CityRepositoryImpl(SessionFactory factory){
        this.factory = factory;
    }

    @Override
    public List<City> getAll() {
        try (Session session = factory.openSession()){
            Query<City> query = session.createQuery("from City");
            return query.list();
        }
    }

    @Override
    public City getByName(String name) {
        try (Session session = factory.openSession()) {
            Query<City> query = session.createQuery("from City where name = :name");
            query.setParameter("name", name);
            City result = query.list().get(0);
            if(result == null){
                throw new EntityNotFoundException(String.format("City with name %s not found!", name));
            }
            return result;
        }
    }

    @Override
    public List<City> filterByDistrictId(int districtId) {
        try (Session session = factory.openSession()) {
            Query<City> query = session.createQuery("from City where district.id = :districtId ");
            query.setParameter("districtId", districtId);

            return query.list();
        }
    }

    @Override
    public City getById(int id) {
        try(Session session = factory.openSession()){
            City city = session.get(City.class, id);
            if (city == null){
                throw new EntityNotFoundException(String.format("City with id %d not found!", id));
            }
            return city;
        }
    }

}
