package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.CarModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarModelRepositoryImpl implements CarModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<CarModel> getByBrandId(int brandId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where brand.id = :brand ");
            query.setParameter("brand", brandId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException(String.format("Car models for brand with id: \"%d\" not found!", brandId));
            }
            return query.list();
        }
    }

    @Override
    public CarModel getById(int modelId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where id = :id ");
            query.setParameter("id", modelId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException(String.format("Car models with id: \"%d\" not found!", modelId));
            }
            return query.list().get(0);
        }
    }

}
