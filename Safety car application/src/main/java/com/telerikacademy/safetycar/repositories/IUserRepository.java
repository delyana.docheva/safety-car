package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.models.verification.VerificationToken;

public interface IUserRepository {
    void saveVerificationToken(VerificationToken token);

    VerificationToken getVerificationToken(String token);

    void savePasswordResetToken(PasswordResetToken token);

    PasswordResetToken getPasswordResetToken(String token);
}
