package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CarBrand;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarBrandRepositoryImpl implements CarBrandRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarBrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarBrand> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarBrand> query = session.createQuery("from CarBrand");
            return query.list();
        }
    }
}
