package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandRepository {
    List<CarBrand> getAll();
}
