package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.District;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DistrictRepositoryImpl implements DistrictRepository {
    SessionFactory factory;

    @Autowired
    public DistrictRepositoryImpl(SessionFactory factory){
        this.factory = factory;
    }

    @Override
    public List<District> getAll() {
        try (Session session = factory.openSession()){
            Query<District> query = session.createQuery("from District");
            return query.list();
        }
    }

    @Override
    public District getByName(String name) {
        try (Session session = factory.openSession()) {
            Query<District> query = session.createQuery("from District where name = :name");
            query.setParameter("name", name);
            District result = query.list().get(0);
            if(result == null){
                throw new EntityNotFoundException(String.format("District with name %s not found!", name));
            }
            return result;
        }
    }

    @Override
    public List<District> filterByCountryId(int countryId) {
        try (Session session = factory.openSession()) {
            Query<District> query = session.createQuery("from District where country.id = :countryId ");
            query.setParameter("countryId", countryId);

            return query.list();
        }
    }

    @Override
    public District getById(int id) {
        try(Session session = factory.openSession()){
            District district = session.get(District.class, id);
            if (district == null){
                throw new EntityNotFoundException(String.format("District with id %d not found!", id));
            }
            return district;
        }
    }
}
