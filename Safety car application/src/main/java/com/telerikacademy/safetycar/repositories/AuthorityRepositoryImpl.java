package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AuthorityRepositoryImpl implements AuthorityRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AuthorityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Authority> getAllUserAuthorities(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Authority> query = session.createQuery("from Authority where authorityId.userId = :id", Authority.class);
            query.setParameter("id", userId);
            return query.list();
        }
    }

    @Override
    public void updateUsername(User user) {

        List<Authority> authorities = getAllUserAuthorities(user.getId());
        for (Authority authority : authorities) {
            authority.setUsername(user.getUsername());

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.update(authority);
                session.getTransaction().commit();
            }
        }
    }
}
