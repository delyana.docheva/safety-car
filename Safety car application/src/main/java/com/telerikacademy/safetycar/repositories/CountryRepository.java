package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();
}
