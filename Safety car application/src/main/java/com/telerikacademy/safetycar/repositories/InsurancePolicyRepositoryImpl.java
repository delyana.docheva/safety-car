package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InsurancePolicyRepositoryImpl implements InsurancePolicyRepository {
    public static final String NOT_FOUND = "Insurance policy with id %d not found!";

    private final SessionFactory sessionFactory;
    private final PolicyStatusRepository policyStatusRepository;

    @Autowired
    public InsurancePolicyRepositoryImpl(SessionFactory sessionFactory, PolicyStatusRepository policyStatusRepository) {
        this.sessionFactory = sessionFactory;
        this.policyStatusRepository = policyStatusRepository;
    }

    @Override
    public Optional<InsurancePolicy> getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Optional<InsurancePolicy> policy = Optional.ofNullable(session.get(InsurancePolicy.class, id));
            if (policy.isEmpty()) {
                throw new EntityNotFoundException(String.format(NOT_FOUND, id));
            }
            return policy;
        }
    }

    @Override
    public List<InsurancePolicy> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<InsurancePolicy> query = session.createQuery("from InsurancePolicy", InsurancePolicy.class);
            return query.list();
        }
    }

    @Override
    public List<InsurancePolicy> getAllActive() {
        try (Session session = sessionFactory.openSession()) {
            int policyStatusId = policyStatusRepository.getByType(PolicyStatusType.ACTIVE).getId();
            Query<InsurancePolicy> query = session.createQuery("from InsurancePolicy where status.id = :id", InsurancePolicy.class);
            query.setParameter("id", policyStatusId);
            return query.list();
        }
    }

    @Override
    public List<InsurancePolicy> getPoliciesForUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<InsurancePolicy> query = session.createQuery("FROM InsurancePolicy where owner.id = :id order by id" +
                    " desc");
            query.setParameter("id", userId);
            return query.list();
        }
    }

    @Override
    public void update(InsurancePolicy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policy);
            session.update(policy.getCar());
            session.update(policy.getOwner());
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(InsurancePolicy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(policy);
            session.getTransaction().commit();
        }
    }

    @Override
    public int save(InsurancePolicy toSave) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            int id = (int) session.save(toSave);
            session.getTransaction().commit();
            return id;
        }
    }

    @Override
    public List<InsurancePolicy> getAllRequestedPolicies() {
        try (Session session = sessionFactory.openSession()) {
            Query<InsurancePolicy> query = session.createQuery(
                    "FROM InsurancePolicy where status != :statusGenerated order by id desc");
            query.setParameter("statusGenerated", policyStatusRepository.getByType(PolicyStatusType.GENERATED));
            return query.list();
        }
    }

    @Override
    public List<InsurancePolicy> search(String userEmail, String firstName, String lastName, int policyId, int premiumMin, int premiumMax, int statusId
            , Date requestedDateFrom, Date requestedDateTo, LocalDate startDateFrom, LocalDate startDateTo) {
        try (Session session = sessionFactory.openSession()) {

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<InsurancePolicy> criteriaQuery = criteriaBuilder.createQuery(InsurancePolicy.class);
            Root<InsurancePolicy> insurancePolicy = criteriaQuery.from(InsurancePolicy.class);
            Join<InsurancePolicy, User> users = insurancePolicy.join("owner", JoinType.LEFT);
            Join<InsurancePolicy, PolicyStatus> statuses = insurancePolicy.join("status");

            Predicate isRequestedPredicate = criteriaBuilder.notEqual(statuses.get("id"), 1);

            List<Predicate> predicatesList = new ArrayList<>();
            if (statusId == 0) {
                predicatesList.add(isRequestedPredicate);
            }

            generatePredicates(userEmail, firstName, lastName, policyId, premiumMin, premiumMax, statusId, requestedDateFrom,
                    requestedDateTo, startDateFrom, startDateTo, criteriaBuilder, insurancePolicy, predicatesList, users, statuses);


            criteriaQuery.where(predicatesList.toArray(new Predicate[]{}));

            TypedQuery<InsurancePolicy> query = session.createQuery(criteriaQuery);

            return query.getResultList().stream().distinct().collect(Collectors.toList());
        }
    }

    private void generatePredicates(String userEmail, String firstName, String lastName, int policyId, int premiumMin, int premiumMax, int statusId,
                                    Date requestedDateFrom, Date requestedDateTo, LocalDate startDateFrom, LocalDate startDateTo,
                                    CriteriaBuilder criteriaBuilder, Root<InsurancePolicy> policy,
                                    List<Predicate> predicatesList, Join<InsurancePolicy, User> users,
                                    Join<InsurancePolicy, PolicyStatus> statuses) {
        if (!StringUtils.isEmpty(userEmail)) {
            Predicate userEmailPredicate = criteriaBuilder.like(users.get("username"), "%" + userEmail + "%");
            predicatesList.add(userEmailPredicate);
        }

        if (!StringUtils.isEmpty(firstName)) {
            Predicate firstNamePredicate = criteriaBuilder.like(users.get("firstName"), "%" + firstName + "%");
            predicatesList.add(firstNamePredicate);
        }

        if (!StringUtils.isEmpty(lastName)) {
            Predicate lastNamePredicate = criteriaBuilder.like(users.get("lastName"), "%" + lastName + "%");
            predicatesList.add(lastNamePredicate);
        }

        if (policyId != 0) {
            Predicate policyIdPredicate = criteriaBuilder.equal(policy.get("id"), policyId);
            predicatesList.add(policyIdPredicate);
        }

        if (premiumMin != 0 && premiumMax != 0) {
            Predicate countryPredicate = criteriaBuilder.between(policy.get("premiumAmount"), premiumMin, premiumMax);
            predicatesList.add(countryPredicate);
        }

        if (premiumMin != 0 && premiumMax == 0) {
            Predicate countryPredicate = criteriaBuilder.between(policy.get("premiumAmount"), premiumMin, 10000);
            predicatesList.add(countryPredicate);
        }

        if (premiumMin == 0 && premiumMax != 0) {
            Predicate countryPredicate = criteriaBuilder.between(policy.get("premiumAmount"), 0, premiumMax);
            predicatesList.add(countryPredicate);
        }

        if (statusId != 0) {
            Predicate statusPredicate = criteriaBuilder.equal(statuses.get("id"), statusId);
            predicatesList.add(statusPredicate);
        }

        if (requestedDateFrom != null && requestedDateTo != null) {
            Predicate requestedDatePredicate = criteriaBuilder.between(policy.get("requestedDate"), requestedDateFrom, requestedDateTo);
            predicatesList.add(requestedDatePredicate);
        }

        if (startDateFrom != null && startDateTo != null) {
            Predicate startDatePredicate = criteriaBuilder.between(policy.get("startDate"), startDateFrom, startDateTo);
            predicatesList.add(startDatePredicate);
        }
    }
}

