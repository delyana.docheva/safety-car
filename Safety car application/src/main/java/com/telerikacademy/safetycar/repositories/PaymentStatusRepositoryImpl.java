package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.PaymentStatus;
import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.models.enums.PaymentStatusType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentStatusRepositoryImpl implements PaymentStatusRepository {
    public static final String NOT_FOUND_BY_ID = "Payment status with id %d not found!";
    public static final String NOT_FOUNT_BY_NAME = "Payment status with name %s not found!";

    private final SessionFactory sessionFactory;

    @Autowired
    public PaymentStatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PaymentStatus getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PaymentStatus status = session.get(PaymentStatus.class, id);
            if (status == null) {
                throw new EntityNotFoundException(String.format(NOT_FOUND_BY_ID, id));
            }
            return status;
        }
    }

    @Override
    public PaymentStatus getByType(PaymentStatusType type) {
        try (Session session = sessionFactory.openSession()) {
            Query<PaymentStatus> query = session.createQuery("from PaymentStatus where type = :name",
                                                                        PaymentStatus.class);
            query.setParameter("name", type);
            PaymentStatus result = query.list().get(0);
            if(result == null){
                throw new EntityNotFoundException(String.format(NOT_FOUNT_BY_NAME, type));
            }
            return result;
        }
    }

    @Override
    public List<PaymentStatus> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PaymentStatus> query = session.createQuery("from PaymentStatus", PaymentStatus.class);
            return query.list();
        }
    }
}
