package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.District;

import java.util.List;

public interface DistrictRepository {
    District getById(int id);

    List<District> getAll();

    District getByName(String name);

    List<District> filterByCountryId(int countryId);
}
