package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.PaymentStatus;
import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.models.enums.PaymentStatusType;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;

import java.util.List;

public interface PaymentStatusRepository {
    PaymentStatus getById(int id);

    PaymentStatus getByType(PaymentStatusType type);

    List<PaymentStatus> getAll();
}
