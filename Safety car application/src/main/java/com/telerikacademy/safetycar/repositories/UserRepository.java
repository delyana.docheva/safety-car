package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends IUserRepository{

    void create(User item);

    void update(User item);

    void delete(User item);

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    List<User> getActiveUsersByUsername(String username);

    List<User> filterByName(Optional<String> username);

    List<User> getAllActiveUsers();

    List<User> search(String username, String phone, int orderBy);

    boolean existsByName(String name);

    boolean existsByNameActive(String name);

}
