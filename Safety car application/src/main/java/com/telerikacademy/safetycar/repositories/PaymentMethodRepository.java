package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.PaymentMethod;
import com.telerikacademy.safetycar.models.PaymentStatus;
import com.telerikacademy.safetycar.models.enums.PaymentMethodType;
import com.telerikacademy.safetycar.models.enums.PaymentStatusType;

import java.util.List;

public interface PaymentMethodRepository {
    PaymentMethod getById(int id);

    PaymentMethod getByType(PaymentMethodType type);

    List<PaymentMethod> getAll();
}
