package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.enums.CoefficientType;

public interface PremiumCalculationRepository {

    double getBaseAmount(int cubicCapacity, int carAge);

    double getCoefficient(CoefficientType coefficientType);

}
