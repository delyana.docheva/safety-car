package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.enums.CoefficientType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PremiumCalculationRepositoryImpl implements PremiumCalculationRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PremiumCalculationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public double getBaseAmount(int cubicCapacity, int carAge) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Double> query = session.createSQLQuery("" +
                    "select base_amount " +
                    "from calculation_base_amount " +
                    "where cc_min <= :cubicCapacity " +
                    "and cc_max >= :cubicCapacity " +
                    "and car_age_min <= :carAge " +
                    "and car_age_max >= :carAge");
            query.setParameter("cubicCapacity", cubicCapacity);
            query.setParameter("carAge", carAge);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException(String.format("No base amount found for cubic capacity %d and car " +
                        "age %d", cubicCapacity, carAge));
            }
            return query.list().get(0);
        }
    }

    @Override
    public double getCoefficient(CoefficientType coefficientType) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Double> query = session.createSQLQuery("select value from coefficients where name = " +
                    ":coefficientType");
            query.setParameter("coefficientType", coefficientType.toString().toLowerCase());
            if (query.list().size() == 0) {
                throw new EntityNotFoundException(String.format("No value found for %s",
                        coefficientType.toString().toLowerCase()));
            }
            return query.list().get(0);
        }
    }

}
