package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.City;

import java.util.List;

public interface CityRepository {
    City getById(int id);

    List<City> getAll();

    City getByName(String name);

    List<City> filterByDistrictId(int districtId);
}
