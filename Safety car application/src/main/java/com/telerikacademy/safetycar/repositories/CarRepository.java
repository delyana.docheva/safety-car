package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Car;

import java.util.List;

public interface CarRepository {
    int create(Car car);

    Car getById(int id);

    void update(Car car);

    Car getByRegistrationNumber(String registrationNumber);

    List<Car> getAllCarsOfUser(int userId);

    boolean existsByRegNumber(String registrationNumber);
}
