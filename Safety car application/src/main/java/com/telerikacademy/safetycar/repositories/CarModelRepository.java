package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.CarModel;

import java.util.List;

public interface CarModelRepository {
    List<CarModel> getByBrandId(int brandId);

    CarModel getById(int modelId);
}
