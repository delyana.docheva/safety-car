package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;

import java.util.List;

public interface AuthorityRepository {
    List<Authority> getAllUserAuthorities(int userId);

    void updateUsername(User user);
}
