package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Car;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {
    public static final String NOT_FOUND_BY_ID = "Car with id %d not found!";
    public static final String NOT_FOUND_BY_REG_NUMBER = "Car with registration number %s not found!";

    private final SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int create(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            int id = (int) session.save(car);
            session.getTransaction().commit();
            return id;
        }
    }

    public Car getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Car car = session.get(Car.class, id);
            if (car == null) {
                throw new EntityNotFoundException(String.format(NOT_FOUND_BY_ID, id));
            }
            return car;
        }
    }

    @Override
    public void update(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(car);
            session.getTransaction().commit();
        }
    }

    @Override
    public Car getByRegistrationNumber(String registrationNumber) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car c where registrationNumber = :registrationNumber",
                    Car.class);
            query.setParameter("registrationNumber", registrationNumber);
            List<Car> users = query.list();
            if (users.isEmpty()) {
                throw new javax.persistence.EntityNotFoundException(String.format(NOT_FOUND_BY_REG_NUMBER, registrationNumber));
            }
            return users.get(0);
        }
    }

    @Override
    public boolean existsByRegNumber(String registrationNumber) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car c where registrationNumber = :registrationNumber",
                    Car.class);
            query.setParameter("registrationNumber", registrationNumber);
            List<Car> users = query.list();

            return !users.isEmpty();
        }
    }

    @Override
    public List<Car> getAllCarsOfUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car where owner.id = :userId and isDeleted = false",
                    Car.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }
}
