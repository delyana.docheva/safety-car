package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;

import java.util.List;

public interface PolicyStatusRepository {

    PolicyStatus getById(int id);

    PolicyStatus getByType(PolicyStatusType type);

    List<PolicyStatus> getAll();
}
