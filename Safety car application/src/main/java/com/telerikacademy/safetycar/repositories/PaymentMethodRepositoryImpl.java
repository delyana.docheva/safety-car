package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.PaymentMethod;
import com.telerikacademy.safetycar.models.enums.PaymentMethodType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentMethodRepositoryImpl implements PaymentMethodRepository {
    public static final String NOT_FOUND_BY_ID = "Payment method with id %d not found!";
    public static final String NOT_FOUNT_BY_NAME = "Payment method with name %s not found!";

    private final SessionFactory sessionFactory;

    @Autowired
    public PaymentMethodRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PaymentMethod getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PaymentMethod method = session.get(PaymentMethod.class, id);
            if (method == null) {
                throw new EntityNotFoundException(String.format(NOT_FOUND_BY_ID, id));
            }
            return method;
        }
    }

    @Override
    public PaymentMethod getByType(PaymentMethodType type) {
        try (Session session = sessionFactory.openSession()) {
            Query<PaymentMethod> query = session.createQuery("from PaymentMethod where type = :name",
                                                                        PaymentMethod.class);
            query.setParameter("name", type);
            PaymentMethod result = query.list().get(0);
            if(result == null){
                throw new EntityNotFoundException(String.format(NOT_FOUNT_BY_NAME, type));
            }
            return result;
        }
    }

    @Override
    public List<PaymentMethod> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PaymentMethod> query = session.createQuery("from PaymentMethod", PaymentMethod.class);
            return query.list();
        }
    }
}
