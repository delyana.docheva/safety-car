package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.Car;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.models.verification.VerificationToken;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository, IUserRepository {

    public static final String USER_NOT_FOUND_BY_ID = "User with id %d not found";
    public static final String USER_NOT_FOUND_BY_NAME = "User with username %s not found";
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }

    }

    @Override
    public void saveVerificationToken(VerificationToken token) {
        Session session = sessionFactory.openSession();
        session.save(token);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        try (Session session = sessionFactory.openSession()) {

            Query<VerificationToken> query = session.createQuery("from VerificationToken where token = :token",
                    VerificationToken.class);
            query.setParameter("token", token);
            return query.list().get(0);
        }
    }

    @Override
    public void savePasswordResetToken(PasswordResetToken token) {
        Session session = sessionFactory.openSession();
        session.save(token);
    }

    @Override
    public PasswordResetToken getPasswordResetToken(String token) {
        try (Session session = sessionFactory.openSession()) {

            Query<PasswordResetToken> query = session.createQuery("from PasswordResetToken where token = :token",
                    PasswordResetToken.class);
            query.setParameter("token", token);
            return query.list().get(0);
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(String.format(USER_NOT_FOUND_BY_ID, id));
            }
            return user;
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException(String.format(USER_NOT_FOUND_BY_NAME, username));
            }

            return users.get(0);
        }
    }

    @Override
    public List<User> getActiveUsersByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where username = :username and enabled = true " +
                    "and isDeleted = false", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();

            return users;
        }
    }

    @Override
    public List<User> filterByName(Optional<String> username) {
        if (username.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session
                    .createQuery("from User where username like concat('%',:username,'%') order by username desc",
                            User.class);
            query.setParameter("username", username.get());
            return query.list();
        }
    }

    @Override
    public boolean existsByName(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();

            return !users.isEmpty();
        }
    }

    @Override
    public boolean existsByNameActive(String name) {
        return !getActiveUsersByUsername(name).isEmpty();
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User",
                    User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllActiveUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where enabled = true and isDeleted = false",
                    User.class);
            return query.list();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> search(String username, String phone, int orderBy) {

        try (Session session = sessionFactory.openSession()) {

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
            Root<User> userInfoRoot = criteriaQuery.from(User.class);



            List<Predicate> predicatesList = new ArrayList<>();

            generatePredicates(username, phone, criteriaBuilder, userInfoRoot, predicatesList);

            criteriaQuery.where(predicatesList.toArray(new Predicate[]{}));

            if (orderBy != 0) {
                orderByGenerator(orderBy, criteriaBuilder, criteriaQuery, userInfoRoot);
            }

            TypedQuery<User> query = session.createQuery(criteriaQuery);

            List<User> result = query.getResultList();

            return query.getResultList();
        }
    }

    private void generatePredicates(String username, String phone, CriteriaBuilder criteriaBuilder,
                                    Root<User> userInfoRoot, List<Predicate> predicatesList) {
        if (!StringUtils.isEmpty(username)) {
            Predicate namePredicate = criteriaBuilder.like(userInfoRoot.get("username"), "%" + username + "%");
            predicatesList.add(namePredicate);
        }

        if (!StringUtils.isEmpty(phone)) {
            Predicate phonePredicate = criteriaBuilder.equal(userInfoRoot.get("phone"), phone);
            predicatesList.add(phonePredicate);
        }
    }

    private void orderByGenerator(int orderBy, CriteriaBuilder criteriaBuilder, CriteriaQuery<User> criteriaQuery,
                                  Root<User> user) {
        if (orderBy == 1) {
            criteriaQuery.orderBy(criteriaBuilder.asc(user.get("username")));
        }
        if (orderBy == 2) {
            criteriaQuery.orderBy(criteriaBuilder.desc(user.get("username")));
        }

    }
}
