package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.InsurancePolicy;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface InsurancePolicyRepository {

    Optional<InsurancePolicy> getById(int id);

    List<InsurancePolicy> getPoliciesForUser(int userId);

    void update(InsurancePolicy policy);

    void delete(InsurancePolicy policy);

    int save(InsurancePolicy toPersist);

    List<InsurancePolicy> getAllRequestedPolicies();

    List<InsurancePolicy> getAll();

    List<InsurancePolicy> getAllActive();

    List<InsurancePolicy> search(String userEmail, String firstName, String lastName, int policyId, int premiumMin, int premiumMax,
                                 int statusId, Date requestedDateFrom, Date requestedDateTo, LocalDate startDateFrom,
                                 LocalDate startDateTo);
}
