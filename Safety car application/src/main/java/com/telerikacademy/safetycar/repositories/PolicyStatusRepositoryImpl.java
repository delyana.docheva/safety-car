package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PolicyStatusRepositoryImpl implements PolicyStatusRepository {
    public static final String NOT_FOUND = "Insurance status with id %d not found!";

    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyStatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public PolicyStatus getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PolicyStatus status = session.get(PolicyStatus.class, id);
            if (status == null) {
                throw new EntityNotFoundException(String.format(NOT_FOUND, id));
            }
            return status;
        }
    }

    @Override
    public PolicyStatus getByType(PolicyStatusType type) {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyStatus> query = session.createQuery("from PolicyStatus where type = :name",
                                                                        PolicyStatus.class);
            query.setParameter("name", type);
            PolicyStatus result = query.list().get(0);
            if(result == null){
                throw new EntityNotFoundException(String.format("Policy status with name %s not found!", type));
            }
            return result;
        }
    }

    @Override
    public List<PolicyStatus> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyStatus> query = session.createQuery("from PolicyStatus", PolicyStatus.class);
            return query.list();
        }
    }
}
