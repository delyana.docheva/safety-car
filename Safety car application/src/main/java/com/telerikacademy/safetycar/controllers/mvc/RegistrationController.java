package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyCalculateOfferDTO;
import com.telerikacademy.safetycar.models.dtos.Mapper;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDTO;
import com.telerikacademy.safetycar.models.verification.OnExpiredTokenEvent;
import com.telerikacademy.safetycar.models.verification.OnRegistrationCompleteEvent;
import com.telerikacademy.safetycar.models.verification.VerificationToken;
import com.telerikacademy.safetycar.services.CityService;
import com.telerikacademy.safetycar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;

@Controller
public class RegistrationController {

    private final UserService userService;
    private final Mapper mapper;
    private final ApplicationEventPublisher eventPublisher;
    private final MessageSource messages;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService,
                                  ApplicationEventPublisher eventPublisher, Mapper mapper, CityService cityService,
                                  @Qualifier("messageSource") MessageSource messages) {
        this.userService = userService;
        this.eventPublisher = eventPublisher;
        this.mapper = mapper;
        this.messages = messages;
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserRegistrationDTO userRegistrationDTO,
                               BindingResult bindingResult, Model model, HttpServletRequest request) {
        try {
            if (bindingResult.hasErrors()) {
                return "register";
            }

            User user = mapper.fromUserRegDTO(userRegistrationDTO);

            userService.create(user);

            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, request.getLocale(), appUrl));
            model.addAttribute("calculateOfferDTO", new InsurancePolicyCalculateOfferDTO());

            return "redirect:/";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "error.dto", e.getMessage());

            return "register";
        }
    }

    @GetMapping("/confirm-registration")
    public String confirmRegistration(HttpServletRequest request, Model model, @RequestParam("token") String token) {
        Locale locale = request.getLocale();
        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            String message = "Missing token.";
            model.addAttribute("message", message);
            return "error-400";
        }

        User user = verificationToken.getUser();
        Calendar calendar = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
            String message = "The link for your registration confirmation has expired. Please check your email for new one.";

            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnExpiredTokenEvent(user, request.getLocale(), appUrl));

            model.addAttribute("message", message);
            return "error-400";
        }

        user.setEnabled(true);
        userService.enableRegisteredUser(user, verificationToken);
        return "login";
    }
}
