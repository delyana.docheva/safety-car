package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.PolicyStatus;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyCalculateOfferDTO;
import com.telerikacademy.safetycar.models.dtos.Mapper;
import com.telerikacademy.safetycar.models.dtos.UserEditDTO;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDTO;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.models.verification.OnPasswordResetEvent;
import com.telerikacademy.safetycar.models.verification.PasswordResetToken;
import com.telerikacademy.safetycar.services.CarService;
import com.telerikacademy.safetycar.services.CountryService;
import com.telerikacademy.safetycar.services.InsurancePolicyService;
import com.telerikacademy.safetycar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
public class UserController {
    private final UserService userService;
    private final Mapper mapper;
    private final CountryService countryService;
    private final ApplicationEventPublisher eventPublisher;
    private final CarService carService;
    private final InsurancePolicyService policyService;

    @Autowired
    public UserController(UserService userService, Mapper mapper, CountryService countryService,
                          ApplicationEventPublisher eventPublisher, CarService carService,
                          InsurancePolicyService policyService) {
        this.userService = userService;
        this.mapper = mapper;
        this.countryService = countryService;
        this.eventPublisher = eventPublisher;
        this.carService = carService;
        this.policyService = policyService;
    }

    @GetMapping("/user")
    public String showProfilePage(Model model, Principal principal) {

        User user = userService.getByUsername(principal.getName());

        UserEditDTO userEditDTO = mapper.toUserEditDTO(user);
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("userEditDTO", userEditDTO);

        UserRegistrationDTO userRegistrationDTO = mapper.toUserRegDTO(user);
        model.addAttribute("userRegistrationDTO", userRegistrationDTO);

        model.addAttribute("userCars", carService.getAllCarsOfUser(user.getId()));

        return "profile";
    }

    @PostMapping("/user")
    public String handleEditProfilePage(@Valid @ModelAttribute UserEditDTO userEditDTO,
                                        BindingResult bindingResult, Model model, Principal principal) {
        try {
            if (bindingResult.hasErrors()) {
                showUserRegistrationDTO(userEditDTO, model, principal);
                return "profile";
            }

            User user = mapper.fromUserEditDTO(userEditDTO, userService);
            if (user.getHasAccident() != userEditDTO.getHasAccident() ||
                    !user.getBirthDate().equals(LocalDate.parse(userEditDTO.getBirthDate()))) {
                user.setHasAccident(userEditDTO.getHasAccident());
                user.setBirthDate(LocalDate.parse(userEditDTO.getBirthDate()));
                List<InsurancePolicy> usersGeneratedPolicies = getGeneratedPolicies(user);
                for (InsurancePolicy policy : usersGeneratedPolicies) {
                    policyService.changePolicyStatus(PolicyStatusType.CANCELED, policy, principal.getName());
                }
            }

            userService.update(user, principal.getName());

            return "redirect:/user";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "error.dto", e.getMessage());

            return "profile";
        }

    }

    @PostMapping("/user/update-password")
    public String handlePasswordUpdate(@Valid @ModelAttribute UserRegistrationDTO userRegistrationDTO,
                                       BindingResult bindingResult, Model model, Principal principal) {
        if (bindingResult.hasErrors()) {
            showUserEditDTO(model, principal);
            return "profile";
        }

        User user = mapper.fromUserRegDTO(userRegistrationDTO);

        userService.updatePassword(user, principal);

        return "redirect:/user";
    }

    @GetMapping("/user/delete")
    public String handleDeleteProfile(Model model, Principal principal) {

        User user = userService.getByUsername(principal.getName());

        userService.delete(user, principal);

        model.addAttribute("calculateOfferDTO", new InsurancePolicyCalculateOfferDTO());
        return "redirect:/logout";

    }

    @GetMapping("/reset-password")
    public String showPasswordResetPage(Model model, HttpServletRequest request) {
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "password-reset-request";
    }

    @PostMapping("/reset-password")
    public String passwordReset(@ModelAttribute UserRegistrationDTO userRegistrationDTO,
                                BindingResult bindingResult, Model model, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "password-reset-request";
        }

        User user = userService.getByUsername(userRegistrationDTO.getUsername());

        String token = UUID.randomUUID().toString();
        userService.createPasswordResetToken(user, token);

        String appUrl = request.getContextPath();
        eventPublisher.publishEvent(new OnPasswordResetEvent(user, request.getLocale(), appUrl));

        request.getSession().invalidate();

        return "password-reset-request";
    }

    @GetMapping("/confirm-password")
    public String getConfirmPasswordReset(HttpServletRequest request, Model model, @RequestParam("token") String token) {

        if (token.isEmpty()) {
            String message = "Missing token.";
            model.addAttribute("message", message);
            return "error-400";
        }
        PasswordResetToken passwordResetToken = userService.getPasswordResetToken(token);
        User user = passwordResetToken.getUser();
        Calendar calendar = Calendar.getInstance();
        if ((passwordResetToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
            String message = "The link for resetting your password has expired. Please request new.";
            model.addAttribute("message", message);
            return "error-400";
        }

        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();
        userRegistrationDTO.setUsername(user.getUsername());
        model.addAttribute("userRegistrationDTO", userRegistrationDTO);

        return "password-reset";
    }

    @PostMapping("/confirm-password")
    public String confirmPasswordReset(@Valid @ModelAttribute UserRegistrationDTO userRegistrationDTO,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "password-reset";
        }

        User user = mapper.fromUserRegDTO(userRegistrationDTO);

        userService.updatePassword(user);

        return "login";
    }

    private List<InsurancePolicy> getGeneratedPolicies(User user) {
        return policyService.getPoliciesForUser(user.getUsername())
                .stream().filter(p -> p.getStatus().getType().equals(PolicyStatusType.GENERATED))
                .collect(Collectors.toList());
    }

    private void showUserRegistrationDTO(UserEditDTO userEditDTO, Model model, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        model.addAttribute("countries", countryService.getAll());
        UserRegistrationDTO userRegistrationDTO = mapper.toUserRegDTO(userEditDTO);
        model.addAttribute("userRegistrationDTO", userRegistrationDTO);
        model.addAttribute("userCars", carService.getAllCarsOfUser(user.getId()));
    }

    private void showUserEditDTO(Model model, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        UserEditDTO userEditDTO = mapper.toUserEditDTO(user);
        model.addAttribute("userEditDTO", userEditDTO);
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("userCars", carService.getAllCarsOfUser(user.getId()));
    }

}
