package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.services.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/car-model")
public class CarModelRestController {
    private final CarModelService carModelService;

    @Autowired
    public CarModelRestController(CarModelService carModelService) {
        this.carModelService = carModelService;
    }

    @GetMapping
    public List<CarModel> getCarModelsForBrand(@RequestParam(value = "brandId") Integer brandId) {
        return carModelService.getByBrandId(brandId);
    }
}
