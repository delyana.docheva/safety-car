package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.dtos.InsurancePolicyChangeStatusDTO;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicySearchDTO;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.services.InsurancePolicyService;
import com.telerikacademy.safetycar.services.PolicyStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/agent")
public class AgentController {

    private final InsurancePolicyService insurancePolicyService;
    private final PolicyStatusService policyStatusService;

    @Autowired
    public AgentController(InsurancePolicyService insurancePolicyService, PolicyStatusService policyStatusService) {
        this.insurancePolicyService = insurancePolicyService;
        this.policyStatusService = policyStatusService;
    }

    @GetMapping("/requests")
    public String showManageRequestsPage(Model model) {
        prepareManageRequestsPage(model);
        return "manage-requests";
    }

    @PostMapping("/requests/status")
    public String handleStatusChange(@ModelAttribute("changePolicyStatusDTO") InsurancePolicyChangeStatusDTO changePolicyStatusDTO,
                                     Principal principal, Model model) {
        insurancePolicyService.changePolicyStatus(PolicyStatusType.valueOf(changePolicyStatusDTO.getStatus().toUpperCase()),
                insurancePolicyService.getById(changePolicyStatusDTO.getPolicyId()), principal.getName());

        prepareManageRequestsPage(model);
        return "manage-requests";
    }

    @PostMapping("/requests/search")
    public String handlePolicySearch(@ModelAttribute("InsurancePolicySearchDTO") InsurancePolicySearchDTO searchDTO,
                                     Model model) {
        model.addAttribute("statuses", policyStatusService.getAll());
        model.addAttribute("policies", insurancePolicyService.search(searchDTO.getUsername(),
                searchDTO.getFirstName(), searchDTO.getLastName(), searchDTO.getPolicyId(),
                searchDTO.getPremiumMin(), searchDTO.getPremiumMax(), searchDTO.getStatusId(),
                searchDTO.getRequestedDateFrom(), searchDTO.getRequestedDateTo(), searchDTO.getStartDateFrom(),
                searchDTO.getStartDateTo()));
        model.addAttribute("searchDTO", searchDTO);
        return "manage-requests";
    }

    private void prepareManageRequestsPage(Model model) {
        model.addAttribute("policies", insurancePolicyService.getAllRequestedPolicies());
        model.addAttribute("statuses", policyStatusService.getAll());
        model.addAttribute("searchDTO", new InsurancePolicySearchDTO());
    }
}
