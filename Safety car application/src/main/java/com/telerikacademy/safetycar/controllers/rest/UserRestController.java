package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.Mapper;
import com.telerikacademy.safetycar.models.dtos.UserEditDTO;
import com.telerikacademy.safetycar.models.dtos.UserSearchDTO;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDTO;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.services.InsurancePolicyService;
import com.telerikacademy.safetycar.services.UserService;
import com.telerikacademy.safetycar.validation.ValidationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {

    private final UserService userService;
    private final Mapper mapper;
    private final InsurancePolicyService policyService;

    @Autowired
    public UserRestController(UserService userService, Mapper mapper, InsurancePolicyService policyService) {
        this.userService = userService;
        this.mapper = mapper;
        this.policyService = policyService;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return userService.getActiveById(id);
    }

    @GetMapping("/search")
    public List<User> search(@RequestBody UserSearchDTO dto) {
        return userService.search(dto.getUsername(), dto.getPhone(), dto.getOrderBy());
    }

    @PostMapping("/register")
    public User create(@Valid @RequestBody UserRegistrationDTO user) {
        User toCreate = mapper.fromUserRegDTO(user);
        userService.create(toCreate);
        return toCreate;
    }

    @PutMapping
    public User update(@Valid @RequestBody UserEditDTO userEditDTO, Principal principal) {
        User user = mapper.fromUserEditDTO(userEditDTO, userService);
        if (user.getHasAccident() != userEditDTO.getHasAccident()  ||
                !user.getBirthDate().equals(LocalDate.parse(userEditDTO.getBirthDate()))) {
            user.setHasAccident(userEditDTO.getHasAccident());
            user.setBirthDate(LocalDate.parse(userEditDTO.getBirthDate()));
            List<InsurancePolicy> usersGeneratedPolicies = getGeneratedPolicies(user);
            for (InsurancePolicy policy : usersGeneratedPolicies) {
                policyService.changePolicyStatus(PolicyStatusType.CANCELED, policy, principal.getName());
            }
        }
        userService.update(user, principal.getName());
        return userService.getById(user.getId());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, Principal principal) {
        User user = userService.getById(id);
        userService.delete(user, principal);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return ValidationMessage.getExceptionMessage(ex);
    }

    private List<InsurancePolicy> getGeneratedPolicies(User user) {
        return policyService.getPoliciesForUser(user.getUsername())
                .stream().filter(p -> p.getStatus().getType().equals(PolicyStatusType.GENERATED))
                .collect(Collectors.toList());
    }

}
