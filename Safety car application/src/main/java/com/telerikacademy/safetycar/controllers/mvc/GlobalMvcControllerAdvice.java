package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("com.telerikacademy.safetycar.controllers.mvc")
public class GlobalMvcControllerAdvice {

    @ExceptionHandler(BadRequestException.class)
    public String handleBadRequest(BadRequestException e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error-400";
    }

    @ExceptionHandler(InvalidOperationException.class)
    public String handleInvalidOperation(InvalidOperationException e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error-400";
    }

    @ExceptionHandler(UnauthorizedException.class)
    public String handleUnauthorized(UnauthorizedException e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error-401";
    }

    @ExceptionHandler(AccessDeniedException.class)
    public String handleAccessDenied(AccessDeniedException e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error-403";
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public String handleEntityNotFound(EntityNotFoundException e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error-404";
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public String handleDuplicateEntity(DuplicateEntityException e, Model model) {
        model.addAttribute("message", e.getMessage());
        return "error-409";
    }

}
