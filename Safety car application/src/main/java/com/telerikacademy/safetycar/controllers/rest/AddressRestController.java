package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.District;
import com.telerikacademy.safetycar.services.CityService;
import com.telerikacademy.safetycar.services.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AddressRestController {
    private final DistrictService districtService;
    private final CityService cityService;

    @Autowired
    public AddressRestController(CityService cityService, DistrictService districtService) {
        this.districtService = districtService;
        this.cityService = cityService;
    }

    @GetMapping("api/v1/district")
    public List<District> getDistrictForCountry(@RequestParam(value = "countryId") Integer countryId) {
        return districtService.filterByCountryId(countryId);
    }

    @GetMapping("api/v1/city")
    public List<City> getCitiesForDistrict(@RequestParam(value = "districtId") Integer districtId) {
        return cityService.filterByDistrictId(districtId);
    }
}

