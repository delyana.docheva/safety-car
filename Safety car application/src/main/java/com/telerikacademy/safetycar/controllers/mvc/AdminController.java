package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.*;
import com.telerikacademy.safetycar.models.enums.AuthorityType;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.services.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UserService userService;
    private final DistrictService districtService;
    private final Mapper mapper;
    private final CountryService countryService;
    private final CarService carService;
    private final InsurancePolicyService policyService;

    public AdminController(UserService userService, DistrictService districtService, Mapper mapper,
                           CountryService countryService, CarService carService, InsurancePolicyService policyService) {
        this.userService = userService;
        this.districtService = districtService;
        this.mapper = mapper;
        this.countryService = countryService;
        this.carService = carService;
        this.policyService = policyService;
    }

    @GetMapping("/users")
    public String showEditUsers(Model model) {
        List<User> users = userService.getAll();
        model.addAttribute("users", users);
        model.addAttribute("userSearchDTO", new UserSearchDTO());
        model.addAttribute("district", districtService.getAll());
        return "admin-users-list";
    }

    @PostMapping("/users")
    public String searchForUser(Model model, @ModelAttribute UserSearchDTO userSearchDTO) {

        model.addAttribute("users", userService.search(userSearchDTO.getUsername(),
                userSearchDTO.getPhone(), userSearchDTO.getOrderBy()));

        return "admin-users-list";

    }

    @GetMapping("/user-details/{id}")
    public String showProfilePage(Model model, Principal principal, @PathVariable int id) {

        User user = userService.getById(id);

        UserEditDTO userEditDTO = mapper.toUserEditDTO(user);
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("userEditDTO", userEditDTO);

        UserEmailDTO userEmailDTO = new UserEmailDTO();
        user.setUsername(user.getUsername());
        model.addAttribute("userEmailDTO", userEmailDTO);

        model.addAttribute("userCars", carService.getAllCarsOfUser(user.getId()));

        return "admin-edit-user";
    }

    @PostMapping("/user-details/{id}")
    public String handleEditProfilePage(@Valid @ModelAttribute UserEditDTO userEditDTO,
                                        BindingResult bindingResult, Model model, Principal principal,
                                        @PathVariable int id) {
        try {
            if (bindingResult.hasErrors()) {
                showUserEmailDTO(userEditDTO, model, id);
                return "admin-edit-user";
            }

            User user = mapper.fromUserEditDTO(userEditDTO, userService);
            if (user.getHasAccident() != userEditDTO.getHasAccident() ||
                    !user.getBirthDate().equals(LocalDate.parse(userEditDTO.getBirthDate()))) {
                user.setHasAccident(userEditDTO.getHasAccident());
                user.setBirthDate(LocalDate.parse(userEditDTO.getBirthDate()));
                List<InsurancePolicy> usersGeneratedPolicies = getGeneratedPolicies(user);
                for (InsurancePolicy policy : usersGeneratedPolicies) {
                    policyService.changePolicyStatus(PolicyStatusType.CANCELED, policy, principal.getName());
                }
            }
            if (!CollectionUtils.isEmpty(userEditDTO.getAuthorities())) {
                user.setAuthorities(generateSetOfAuthorities(userEditDTO, user));
            }

            userService.update(user, principal.getName());

            return "redirect:/admin/user-details/{id}";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "error.dto", e.getMessage());

            return "admin-edit-user";
        }

    }

    @PostMapping("/user-update-email/{id}")
    public String handleEditUserEmail(@Valid @ModelAttribute UserEmailDTO userEmailDTO,
                                      BindingResult bindingResult, Model model, Principal principal,
                                      @PathVariable int id) {
        try {
            if (bindingResult.hasErrors()) {
                showUserEditDTO(model, id);
                return "admin-edit-user";
            }

            User user = userService.getById(id);
            user.setUsername(userEmailDTO.getUsername());

            userService.updateEmail(user, principal.getName());

            return "redirect:/admin/user-details/{id}";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "error.dto", e.getMessage());

            return "admin-edit-user";
        }
    }

    private void showUserEditDTO(Model model, int id) {
        User user = userService.getById(id);
        UserEditDTO userEditDTO = mapper.toUserEditDTO(user);
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("userEditDTO", userEditDTO);
        model.addAttribute("userCars", carService.getAllCarsOfUser(user.getId()));
    }

    private Set<Authority> generateSetOfAuthorities(UserEditDTO userEditDTO, User user) {
        Set<Authority> authorities = new HashSet<>();
        for (AuthorityType authorityType : userEditDTO.getAuthorities()) {
            authorities.add(new Authority(user.getId(), user.getUsername(), authorityType.name()));
        }
        return authorities;
    }

    private void showUserEmailDTO(UserEditDTO userEditDTO, Model model, int id) {
        User user = userService.getById(id);
        UserEmailDTO userEmailDTO = new UserEmailDTO();
        userEditDTO.setUsername(userEditDTO.getUsername());
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("userEmailDTO", userEmailDTO);
        model.addAttribute("userCars", carService.getAllCarsOfUser(user.getId()));
    }

    private List<InsurancePolicy> getGeneratedPolicies(User user) {
        return policyService.getPoliciesForUser(user.getUsername())
                .stream().filter(p -> p.getStatus().getType().equals(PolicyStatusType.GENERATED))
                .collect(Collectors.toList());
    }
}
