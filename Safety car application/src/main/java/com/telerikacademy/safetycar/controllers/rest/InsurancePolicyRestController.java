package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyCalculateOfferDTO;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyRequestDTO;
import com.telerikacademy.safetycar.models.dtos.Mapper;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.services.InsurancePolicyService;
import com.telerikacademy.safetycar.services.UserService;
import com.telerikacademy.safetycar.validation.ValidationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/policies")
public class InsurancePolicyRestController {

    private final InsurancePolicyService insurancePolicyService;
    private final UserService userService;
    private final Mapper mapper;

    @Autowired
    public InsurancePolicyRestController(InsurancePolicyService insurancePolicyService, UserService userService,
                                         Mapper mapper) {
        this.insurancePolicyService = insurancePolicyService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public InsurancePolicy getById(@PathVariable int id) {
        return insurancePolicyService.getById(id);
    }

    @PostMapping("/offer")
    public InsurancePolicyCalculateOfferDTO calculateOffer(@Valid @RequestBody InsurancePolicyCalculateOfferDTO offerDTO, Principal principal) {
        InsurancePolicy toCalculate = mapper.fromDto(offerDTO, Optional.ofNullable(principal));
        Double premium = insurancePolicyService.calculateExpectedPremium(mapper.fromDto(offerDTO,
                Optional.ofNullable(principal)));
        DecimalFormat format = new DecimalFormat("0.##");
        offerDTO.setExpectedPremium(Double.parseDouble(format.format(premium)));
        return offerDTO;
    }

    @PostMapping
    public void saveOffer(@Valid @RequestBody InsurancePolicyCalculateOfferDTO offerDTO,
                          Principal principal) {
        insurancePolicyService.save(mapper.fromDto(offerDTO, Optional.ofNullable(principal)),
                userService.getByUsername(principal.getName()));
    }

    @PostMapping("/request")
    public InsurancePolicyRequestDTO requestPolicy(@Valid @RequestPart("dto") InsurancePolicyRequestDTO requestDTO,
                                                   @RequestPart("file") MultipartFile carRegistrationImage,
                                                   Principal principal) throws IOException {
        InsurancePolicy toRequest = mapper.fromDto(requestDTO, carRegistrationImage);
        insurancePolicyService.requestPolicy(toRequest, principal.getName());
        requestDTO.setStatus(PolicyStatusType.REQUESTED);
        return requestDTO;
    }

    @PutMapping
    public void update(@RequestBody InsurancePolicy policy, Principal principal) {
        insurancePolicyService.update(policy, principal.getName());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, Principal principal) {
        insurancePolicyService.delete(id, principal.getName());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return ValidationMessage.getExceptionMessage(ex);
    }
}
