package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyCalculateOfferDTO;
import com.telerikacademy.safetycar.models.dtos.Mapper;
import com.telerikacademy.safetycar.services.CarBrandService;
import com.telerikacademy.safetycar.services.InsurancePolicyService;
import com.telerikacademy.safetycar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Optional;

@Controller("/")
public class HomeController {
    public static final String SESSION_OFFER = "Session offer DTO";

    private final CarBrandService carBrandService;
    private final InsurancePolicyService insurancePolicyService;
    private final Mapper mapper;
    private final UserService userService;

    @Autowired
    public HomeController(CarBrandService carBrandService, InsurancePolicyService insurancePolicyService,
                          Mapper mapper, UserService userService) {
        this.carBrandService = carBrandService;
        this.insurancePolicyService = insurancePolicyService;
        this.mapper = mapper;
        this.userService = userService;
    }

    @GetMapping
    public String showHomePage(Model model, HttpServletRequest request, Principal principal) {
        if (principal != null) {
            User logged = userService.getByUsername(principal.getName());
            InsurancePolicyCalculateOfferDTO dto = new InsurancePolicyCalculateOfferDTO();
            if (logged.getBirthDate() != null){
                dto.setBirthDate(String.valueOf(logged.getBirthDate()));
            } else {
                dto.setBirthDate("");
            }
            dto.setHasAccident(logged.getHasAccident());
            prepareHomePage(model, Optional.of(dto));
            return "index";
        }
        prepareHomePage(model, Optional.empty());
        return "index";
    }

    @PostMapping
    public String handleSimulateOffer(@Valid @ModelAttribute("calculateOfferDTO") InsurancePolicyCalculateOfferDTO calculateOfferDTO,
                                      BindingResult bindingResult, Model model,
                                      HttpServletRequest request, Principal principal) {
        if (bindingResult.hasErrors()) {
            prepareHomePage(model, Optional.of(calculateOfferDTO));
            return "index";
        }
        request.getSession().setAttribute(SESSION_OFFER, calculateOfferDTO);
        calculateOfferDTO = (InsurancePolicyCalculateOfferDTO) request.getSession().getAttribute(SESSION_OFFER);
        Double premium = insurancePolicyService.calculateExpectedPremium(mapper.fromDto(calculateOfferDTO, Optional.ofNullable(principal)));
        DecimalFormat format = new DecimalFormat("0.##");
        calculateOfferDTO.setExpectedPremium(Double.parseDouble(format.format(premium)));
        calculateOfferDTO.setCarIsInRequestedPolicy(insurancePolicyService
                .requestedPoliciesByCarRegNumber(calculateOfferDTO.getRegistrationNumber()));

        prepareHomePage(model, Optional.of(calculateOfferDTO));
        return "index";
    }

    @PostMapping("/save-offer")
    public String handleSaveOffer(Model model, HttpServletRequest request, Principal principal) {
        InsurancePolicyCalculateOfferDTO dto =
                (InsurancePolicyCalculateOfferDTO) request.getSession().getAttribute(SESSION_OFFER);
        User loggedUser = userService.getByUsername(principal.getName());

        LocalDate birthdate = LocalDate.parse(dto.getBirthDate());
        loggedUser.setBirthDate(birthdate);

        userService.update(loggedUser, principal.getName());
        insurancePolicyService.save(mapper.fromDto(dto, Optional.of(principal)), loggedUser);

        request.getSession().removeAttribute(SESSION_OFFER);

        model.addAttribute("policies", insurancePolicyService.getPoliciesForUser(principal.getName()));
        return "my-policies";
    }

    private void prepareHomePage(Model model, Optional<InsurancePolicyCalculateOfferDTO> calculateOfferDTO) {
        if (calculateOfferDTO.isPresent()) {
            model.addAttribute("calculateOfferDTO", calculateOfferDTO.get());

        } else {
            model.addAttribute("calculateOfferDTO", new InsurancePolicyCalculateOfferDTO());
        }
        model.addAttribute("brands", carBrandService.getAll());
    }

}
