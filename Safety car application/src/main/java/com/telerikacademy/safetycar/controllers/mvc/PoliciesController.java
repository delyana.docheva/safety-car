package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.InsurancePolicy;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyChangeStatusDTO;
import com.telerikacademy.safetycar.models.dtos.InsurancePolicyRequestDTO;
import com.telerikacademy.safetycar.models.dtos.Mapper;
import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import com.telerikacademy.safetycar.services.CountryService;
import com.telerikacademy.safetycar.services.InsurancePolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;

@Controller
@RequestMapping("/policy")
public class PoliciesController {
    private final InsurancePolicyService insurancePolicyService;
    private final CountryService countryService;
    private final Mapper mapper;

    @Autowired
    public PoliciesController(InsurancePolicyService insurancePolicyService, CountryService countryService,
                              Mapper mapper) {
        this.insurancePolicyService = insurancePolicyService;
        this.countryService = countryService;
        this.mapper = mapper;
    }

    @GetMapping("/my-policies")
    public String showMyPoliciesPage(Model model, Principal principal) {
        prepareMyPoliciesPage(principal, model);
        return "my-policies";
    }

    @GetMapping("/request/{id}")
    public String showRequestPolicyPage(Model model, @PathVariable int id, Principal principal) {
        InsurancePolicy policy = insurancePolicyService.getById(id);
        insurancePolicyService.checkIfLoggedUserIsAllowedToReadPolicy(policy.getOwner().getUsername(), principal.getName());
        InsurancePolicyRequestDTO dto = mapper.fromPolicy(policy);
        if (!policy.getOwner().getHasAccident() &&
                policy.getOwner().getHasAccident() != insurancePolicyService.validateAccidentsInput()){
            dto.setValidAccidentsInput(false);
        }
        prepareRequestPolicyPage(model, dto);
        return "request-policy";
    }


    @PostMapping("/request")
    public String handleRequestPolicy(@Valid @ModelAttribute("policyRequestDTO") InsurancePolicyRequestDTO policyRequestDTO,
                                      BindingResult bindingResult, Model model,
                                      Principal principal, @RequestParam("file") MultipartFile carRegistrationImage) throws IOException {
       String policyOwnerUsername = insurancePolicyService.getById(policyRequestDTO.getId()).getOwner().getUsername();
        insurancePolicyService.checkIfLoggedUserIsAllowedToReadPolicy(policyOwnerUsername,principal.getName());
        if (bindingResult.hasErrors()) {
            prepareRequestPolicyPage(model, policyRequestDTO);
            return "request-policy";
        }

        InsurancePolicy requestedPolicy = mapper.fromDto(policyRequestDTO, carRegistrationImage);
        insurancePolicyService.requestPolicy(requestedPolicy, principal.getName());

        prepareMyPoliciesPage(principal, model);
        return "my-policies";
    }

    @GetMapping("/details/{id}")
    public String showPolicyDetailsPage(Model model, @PathVariable int id, Principal principal) {
        InsurancePolicy policy = insurancePolicyService.getById(id);
        insurancePolicyService.checkIfLoggedUserIsAllowedToReadPolicy(policy.getOwner().getUsername(),
                principal.getName());
        model.addAttribute("loggedUsername", principal.getName());
        model.addAttribute("policy", policy);
        model.addAttribute("ownerAge", insurancePolicyService.calculateAge(policy.getOwner().getBirthDate()));
        model.addAttribute("carAge", insurancePolicyService.calculateAge(policy.getCar().getRegistrationDate()));
        model.addAttribute("changePolicyStatusDTO", new InsurancePolicyChangeStatusDTO());

        if (policy.getStatus().getId() > 1) {
            String certificateImage = Base64.getEncoder().encodeToString(policy.getCar().getRegistrationCertificate());
            model.addAttribute("certificate", certificateImage);
        }

        return "policy-details";
    }

    @PostMapping("/my-policies/cancel")
    public String handleCancelPolicy(@ModelAttribute("cancelPolicyDTO") InsurancePolicyChangeStatusDTO cancelPolicyDTO,
                                     Principal principal, Model model) {
        PolicyStatusType status = PolicyStatusType.valueOf(cancelPolicyDTO.getStatus().toUpperCase());
        insurancePolicyService.changePolicyStatus(status, insurancePolicyService.getById(cancelPolicyDTO.getPolicyId()),
                principal.getName());
        prepareMyPoliciesPage(principal, model);
        return "my-policies";
    }

    private void prepareMyPoliciesPage(Principal principal, Model model) {
        model.addAttribute("policies", insurancePolicyService.getPoliciesForUser(principal.getName()));
        model.addAttribute("cancelPolicyDTO", new InsurancePolicyChangeStatusDTO());
    }

    private void prepareRequestPolicyPage(Model model, InsurancePolicyRequestDTO policyRequestDTO) {
        model.addAttribute("policyRequestDTO", policyRequestDTO);
        model.addAttribute("policy", insurancePolicyService.getById(policyRequestDTO.getId()));
        model.addAttribute("countries", countryService.getAll());
    }

}
