package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.models.enums.AuthorityType;
import com.telerikacademy.safetycar.validation.ValidEmail;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Set;

public class UserEditDTO {
    private int id;

    @NotEmpty
    @Size(min = 3, max = 50, message = "Username should be between {min} and {max} characters long.")
    @ValidEmail
    private String username;

    private Set<AuthorityType> authorities;

    @Size(min = 2, max = 20, message = "First name should be between {min} and {max} characters long.")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name should be between {min} and {max} characters long.")
    private String lastName;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String birthDate;

    private String address;

    @Positive
    private int countryId;

    @Positive
    private int districtId;

    @Positive
    private int cityId;

    private String fullAddress;

    @Pattern(regexp = "^\\d{10}$", message = "Phone number must contain 10 digits")
    private String phone;

    private boolean hasAccident;

    private boolean isDeleted;

    private boolean isEnabled;

    public UserEditDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<AuthorityType> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<AuthorityType> authorities) {
        this.authorities = authorities;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getHasAccident() {
        return hasAccident;
    }

    public void setHasAccident(boolean hasAccident) {
        this.hasAccident = hasAccident;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
