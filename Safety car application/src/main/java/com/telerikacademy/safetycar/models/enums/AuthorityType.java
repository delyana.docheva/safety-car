package com.telerikacademy.safetycar.models.enums;

public enum AuthorityType {
    ROLE_USER,
    ROLE_AGENT,
    ROLE_ADMIN;
}
