package com.telerikacademy.safetycar.models;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "authorities")
public class Authority {

    @EmbeddedId
    private AuthorityId authorityId;

    @Column(name = "username")
    private String username;

    public Authority() {
    }

    public Authority(int userId, String username, String authority) {
        this.authorityId = new AuthorityId(userId, authority);
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Authority authority1 = (Authority) o;
        return Objects.equals(username, authority1.username) &&
                Objects.equals(authorityId.getAuthority(), authority1.getAuthority());
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, authorityId);
    }

    public int getUserId() {
        return authorityId.getUserId();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authorityId.getAuthority();
    }
}
