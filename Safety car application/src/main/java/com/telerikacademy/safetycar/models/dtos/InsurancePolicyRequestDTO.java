package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.models.enums.PolicyStatusType;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

public class InsurancePolicyRequestDTO {
    @Positive
    private int id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String startDate;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @NotEmpty
    @Pattern(regexp = "\\d+", message = "Phone number must contain digits only")
    @Length(min = 10, max = 15, message = "Phone number must be between 10 and 15 digits long")
    private String phone;

    private int CountryId;

    private int districtId;

    private int cityId;

    @NotEmpty
    @Length(min = 5, message = "Address must be at least 5 characters long")
    private String postalAddress;

    private PolicyStatusType status;

    private int paymentMethodId;

    private boolean validAccidentsInput;

    public InsurancePolicyRequestDTO() {
        this.validAccidentsInput = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCountryId() {
        return CountryId;
    }

    public void setCountryId(int countryId) {
        CountryId = countryId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public PolicyStatusType getStatus() {
        return status;
    }

    public void setStatus(PolicyStatusType status) {
        this.status = status;
    }

    public int getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(int paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public boolean isValidAccidentsInput() {
        return validAccidentsInput;
    }

    public void setValidAccidentsInput(boolean validAccidentsInput) {
        this.validAccidentsInput = validAccidentsInput;
    }
}
