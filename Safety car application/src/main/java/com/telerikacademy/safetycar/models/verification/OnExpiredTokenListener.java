package com.telerikacademy.safetycar.models.verification;

import com.telerikacademy.safetycar.services.EmailService;
import com.telerikacademy.safetycar.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.telerikacademy.safetycar.models.User;

import java.util.UUID;

@Component
public class OnExpiredTokenListener implements ApplicationListener<OnExpiredTokenEvent> {
    private final IUserService service;
    private final EmailService emailService;

    @Autowired
    public OnExpiredTokenListener(IUserService service, EmailService emailService) {
        this.service = service;
        this.emailService = emailService;
    }

    @Override
    public void onApplicationEvent(OnExpiredTokenEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnExpiredTokenEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createVerificationToken(user, token);

        emailService.generateNewVerificationTokenMail(event, user, token);
    }
}
