package com.telerikacademy.safetycar.models.enums;

public enum PaymentMethodType {
    PREPAID,
    EXTENDED;
}
