package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.enums.AuthorityType;
import com.telerikacademy.safetycar.repositories.PaymentMethodRepository;
import com.telerikacademy.safetycar.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class Mapper {

    private final InsurancePolicyService insurancePolicyService;
    private final CityService cityService;
    private final CarModelService carModelService;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final PaymentMethodRepository paymentMethodRepository;
    private final CarService carService;

    @Autowired
    public Mapper(InsurancePolicyService insurancePolicyService, CityService cityService,
                  CarModelService carModelService, PasswordEncoder passwordEncoder, UserService userService,
                  PaymentMethodRepository paymentMethodRepository, CarService carService) {
        this.insurancePolicyService = insurancePolicyService;
        this.cityService = cityService;
        this.carModelService = carModelService;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.paymentMethodRepository = paymentMethodRepository;
        this.carService = carService;
    }

    public InsurancePolicy fromDto(InsurancePolicyCalculateOfferDTO dto, Optional<Principal> principal) {
        InsurancePolicy policy = new InsurancePolicy();
        policy.setCar(getAnonymousUserCar(dto));
        if (principal.isPresent()) {
            if (userService.getByUsername(principal.get().getName()).getBirthDate() == null) {
                User user = new User();
                user.setBirthDate(LocalDate.parse((dto.getBirthDate())));
                user.setHasAccident(dto.isHasAccident());
                policy.setOwner(user);
                policy.getCar().setOwner(user);
            } else {
                policy.setOwner(userService.getByUsername(principal.get().getName()));
                policy.getCar().setOwner(userService.getByUsername(principal.get().getName()));
            }
        } else {
            policy.setOwner(getAnonymousUser(dto.getBirthDate(), dto.isHasAccident()));
        }
        if (dto.getExpectedPremium() != 0) {
            policy.setPremiumAmount(dto.getExpectedPremium());
        }
        return policy;
    }

    public InsurancePolicy fromDto(InsurancePolicyRequestDTO dto, MultipartFile carRegistrationImage) throws IOException {
        InsurancePolicy result = insurancePolicyService.getById(dto.getId());
        result.setStartDate(LocalDate.parse((dto.getStartDate())));
        result.setPaymentMethod(paymentMethodRepository.getById(dto.getPaymentMethodId()));
        result.getCar().setRegistrationCertificate(carRegistrationImage.getBytes());
        String fileName = carRegistrationImage.getOriginalFilename();
        if (fileName == null) {
            throw new EntityNotFoundException("Could not get registration certificate image");
        }
        result.getCar().setRegistrationCertificateFileType(getImageFileTypeName(fileName));
        User user = result.getOwner();
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setCity(cityService.getById(dto.getCityId()));
        user.setAddress(dto.getPostalAddress());
        user.setPhone(dto.getPhone());
        return result;
    }

    public InsurancePolicyRequestDTO fromPolicy(InsurancePolicy policy) {
        InsurancePolicyRequestDTO result = new InsurancePolicyRequestDTO();
        result.setId(policy.getId());

        User policyOwner = policy.getOwner();
        setExistingPolicyOwner(result, policyOwner);
        return result;
    }

    private void setExistingPolicyOwner(InsurancePolicyRequestDTO result, User policyOwner) {
        if (policyOwner.getFirstName() != null) {
            result.setFirstName(policyOwner.getFirstName());
        }
        if (policyOwner.getLastName() != null) {
            result.setLastName(policyOwner.getLastName());
        }
        if (policyOwner.getPhone() != null) {
            result.setPhone(policyOwner.getPhone());
        }
        if (policyOwner.getAddress() != null) {
            result.setPostalAddress(policyOwner.getAddress());
            City ownerCity = policyOwner.getCity();
            result.setCountryId(ownerCity.getDistrict().getCountry().getId());
            result.setDistrictId(ownerCity.getDistrict().getId());
            result.setCityId(ownerCity.getId());
        }
    }

    public User fromUserRegDTO(UserRegistrationDTO dto) {
        User user;
        if (userService.existByNameActive(dto.getUsername())) {
            user = userService.getByUsername(dto.getUsername());
        } else {
            user = new User();
        }

        user.setUsername(dto.getUsername());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));

        return user;
    }

    public UserRegistrationDTO toUserRegDTO(User user) {
        UserRegistrationDTO userDTO = new UserRegistrationDTO();

        userDTO.setUsername(user.getUsername());

        return userDTO;
    }

    public UserRegistrationDTO toUserRegDTO(UserEditDTO userEditDTO) {
        UserRegistrationDTO userDTO = new UserRegistrationDTO();

        userDTO.setUsername(userEditDTO.getUsername());

        return userDTO;
    }

    public UserViewDTO toUserViewDTO(User user) {
        UserViewDTO userViewDTO = new UserViewDTO();

        userViewDTO.setUsername(user.getUsername());
        userViewDTO.setFirstName(user.getFirstName());
        userViewDTO.setLastName(user.getLastName());
        userViewDTO.setBirthDate(user.getBirthDate());
        userViewDTO.setAddress(user.getAddress());
        userViewDTO.setCity(user.getCity());
        userViewDTO.setPhone(user.getPhone());

        return userViewDTO;
    }

    public User fromUserEditDTO(UserEditDTO userEditDTO, UserService userService) {
        User user = userService.getByUsername(userEditDTO.getUsername());

        user.setUsername(userEditDTO.getUsername());
        user.setFirstName(userEditDTO.getFirstName());
        user.setLastName(userEditDTO.getLastName());
        user.setAddress(userEditDTO.getAddress());
        user.setPhone(userEditDTO.getPhone());

        City city = cityService.getById(userEditDTO.getCityId());
        user.setCity(city);

        return user;
    }

    public UserEditDTO toUserEditDTO(User user) {
        UserEditDTO userEditDTO = new UserEditDTO();

        userEditDTO.setUsername(user.getUsername());
        userEditDTO.setFirstName(user.getFirstName());
        userEditDTO.setLastName(user.getLastName());
        if (user.getBirthDate() != null) {
            userEditDTO.setBirthDate(user.getBirthDate().toString());
        }
        userEditDTO.setPhone(user.getPhone());
        userEditDTO.setHasAccident(user.getHasAccident());

        if (!CollectionUtils.isEmpty(user.getAuthorities())) {
            Set<AuthorityType> result = new HashSet<>();
            for (Authority authority : user.getAuthorities()) {
                AuthorityType authorityType = AuthorityType.valueOf(authority.getAuthority());
                result.add(authorityType);
            }
            userEditDTO.setAuthorities(result);
        }

        if (user.getAddress() != null) {
            userEditDTO.setAddress(user.getAddress());
            userEditDTO.setFullAddress(user.getAddress());
        }

        if (user.getCity() != null) {
            String fullAddress;
            City city = cityService.getById(user.getCity().getId());
            District district = city.getDistrict();

            userEditDTO.setCountryId(district.getCountry().getId());
            userEditDTO.setDistrictId(district.getId());
            userEditDTO.setCityId(user.getCity().getId());
            fullAddress = (district.getCountry().getName() + ", " + district.getName() + ", " + city.getName());
            if (user.getAddress() != null) {
                fullAddress += ", " + user.getAddress();
            }
            userEditDTO.setFullAddress(fullAddress);
        }

        return userEditDTO;
    }

    private User getAnonymousUser(String birthDate, boolean hasAccident) {
        User anonymousUser = new User();
        anonymousUser.setBirthDate(LocalDate.parse(birthDate));
        anonymousUser.setHasAccident(hasAccident);
        return anonymousUser;
    }

    private Car getAnonymousUserCar(InsurancePolicyCalculateOfferDTO dto) {
        if (carService.existsByRegNumber(dto.getRegistrationNumber())) {
            Car car = carService.getByRegistrationNumber(dto.getRegistrationNumber());
            CarModel model = carModelService.getById(dto.getModelId());
            car.setModel(model);
            car.setCubicCapacity(dto.getCubicCapacity());
            car.setRegistrationDate(LocalDate.parse(dto.getRegistrationDate()));
            return car;
        } else {
            Car car = new Car();
            CarModel model = carModelService.getById(dto.getModelId());
            car.setModel(model);
            car.setCubicCapacity(dto.getCubicCapacity());
            car.setRegistrationDate(LocalDate.parse(dto.getRegistrationDate()));
            car.setRegistrationNumber(dto.getRegistrationNumber());
            return car;
        }
    }

    private String getImageFileTypeName(String fileName) {
        String[] nameAndType = fileName.split("\\.");
        return nameAndType[1];
    }
}
