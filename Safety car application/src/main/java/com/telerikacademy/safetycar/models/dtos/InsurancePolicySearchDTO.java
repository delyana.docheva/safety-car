package com.telerikacademy.safetycar.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;


public class InsurancePolicySearchDTO {

    private String username;
    private String firstName;
    private String lastName;
    private int policyId;
    private int premiumMin;
    private int premiumMax;
    private int statusId;

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private Date requestedDateFrom;

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private Date requestedDateTo;

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private LocalDate startDateFrom;

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private LocalDate startDateTo;
    private int orderBy;

    public InsurancePolicySearchDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public int getPremiumMin() {
        return premiumMin;
    }

    public void setPremiumMin(int premiumMin) {
        this.premiumMin = premiumMin;
    }

    public int getPremiumMax() {
        return premiumMax;
    }

    public void setPremiumMax(int premiumMax) {
        this.premiumMax = premiumMax;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Date getRequestedDateFrom() {
        return requestedDateFrom;
    }

    public void setRequestedDateFrom(Date requestedDateFrom) {
        this.requestedDateFrom = requestedDateFrom;
    }

    public Date getRequestedDateTo() {
        return requestedDateTo;
    }

    public void setRequestedDateTo(Date requestedDateTo) {
        this.requestedDateTo = requestedDateTo;
    }

    public LocalDate getStartDateFrom() {
        return startDateFrom;
    }

    public void setStartDateFrom(LocalDate startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    public LocalDate getStartDateTo() {
        return startDateTo;
    }

    public void setStartDateTo(LocalDate startDateTo) {
        this.startDateTo = startDateTo;
    }
}
