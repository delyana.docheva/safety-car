package com.telerikacademy.safetycar.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


public class InsurancePolicyCalculateOfferDTO implements Serializable {
    @Positive(message = "Please select a brand")
    private int brandId;

    @Positive(message = "Please select a model")
    private int modelId;

    @Size(min = 7, max = 8, message = "Registration number should contain between {min} and {max} symbols.")
    private String registrationNumber;

    @Positive(message = "Cubic capacity must be above 0")
    @Max(value = 999999, message = "Cubic capacity cannot be more than 999 999")
    private int cubicCapacity;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String registrationDate;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String birthDate;

    @NotNull
    private boolean hasAccident;

    private double expectedPremium;

    private boolean carIsInRequestedPolicy;

    public InsurancePolicyCalculateOfferDTO() {
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isHasAccident() {
        return hasAccident;
    }

    public void setHasAccident(boolean hasAccident) {
        this.hasAccident = hasAccident;
    }

    public double getExpectedPremium() {
        return expectedPremium;
    }

    public void setExpectedPremium(double expectedPremium) {
        this.expectedPremium = expectedPremium;
    }

    public boolean isCarIsInRequestedPolicy() {
        return carIsInRequestedPolicy;
    }

    public void setCarIsInRequestedPolicy(boolean carIsInRequestedPolicy) {
        this.carIsInRequestedPolicy = carIsInRequestedPolicy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsurancePolicyCalculateOfferDTO that = (InsurancePolicyCalculateOfferDTO) o;
        return brandId == that.brandId &&
                modelId == that.modelId &&
                cubicCapacity == that.cubicCapacity &&
                hasAccident == that.hasAccident &&
                Double.compare(that.expectedPremium, expectedPremium) == 0 &&
                Objects.equals(registrationDate, that.registrationDate) &&
                Objects.equals(birthDate, that.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brandId, modelId, cubicCapacity, registrationDate, birthDate, hasAccident, expectedPremium);
    }
}
