package com.telerikacademy.safetycar.models.enums;

public enum PaymentStatusType {
    PENDING,
    COMPLETE,
    REJECTED,
    DELAYED,
    ON_TIME;
}
