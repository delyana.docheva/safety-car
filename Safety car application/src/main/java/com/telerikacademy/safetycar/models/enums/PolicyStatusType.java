package com.telerikacademy.safetycar.models.enums;

public enum PolicyStatusType {
    GENERATED,
    REQUESTED,
    PENDING,
    APPROVED,
    REJECTED,
    CANCELED,
    ACTIVE,
    EXPIRED;
}
