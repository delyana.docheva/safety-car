package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.validation.ValidEmail;
import com.telerikacademy.safetycar.validation.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserRegistrationDTO {
    @NotEmpty
    @Size(min = 3, max = 50, message = "Username should be between {min} and {max} characters long.")
    @ValidEmail
    private String username;

    @ValidPassword
    private String password;

    private String passwordConfirmation;

    public UserRegistrationDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
