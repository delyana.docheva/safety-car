package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.validation.ValidEmail;
import com.telerikacademy.safetycar.validation.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserEmailDTO {
    @NotEmpty
    @Size(min = 3, max = 50, message = "Username should be between {min} and {max} characters long.")
    @ValidEmail
    private String username;

    public UserEmailDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
