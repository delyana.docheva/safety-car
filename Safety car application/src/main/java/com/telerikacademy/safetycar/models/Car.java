package com.telerikacademy.safetycar.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "model_id")
    @NotNull
    private CarModel model;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "first_registration_date")
    private LocalDate registrationDate;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @Column(name = "registration_certificate_image")
    @Lob
    private byte[] registrationCertificate;

    @Column(name = "registration_certificate_image_filetype")
    private String registrationCertificateFileType;

    @Column(name = "registration_number")
    private String registrationNumber;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    public Car() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getRegistrationCertificate() {
        return registrationCertificate;
    }

    public void setRegistrationCertificate(byte[] registrationCertificate) {
        this.registrationCertificate = registrationCertificate;
    }

    public String getRegistrationCertificateFileType() {
        return registrationCertificateFileType;
    }

    public void setRegistrationCertificateFileType(String registrationCertificateFileType) {
        this.registrationCertificateFileType = registrationCertificateFileType;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
