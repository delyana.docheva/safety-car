package com.telerikacademy.safetycar.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

public class InsurancePolicyChangeStatusDTO {
    @Positive
    private int policyId;
    @NotEmpty
    private String status;

    public InsurancePolicyChangeStatusDTO() {
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
