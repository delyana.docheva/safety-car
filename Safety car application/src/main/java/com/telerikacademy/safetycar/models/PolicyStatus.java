package com.telerikacademy.safetycar.models;

import com.telerikacademy.safetycar.models.enums.PolicyStatusType;

import javax.persistence.*;

@Entity
@Table(name = "status")
public class PolicyStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private PolicyStatusType type;

    public PolicyStatus() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PolicyStatusType getType() {
        return type;
    }

    public void setType(PolicyStatusType type) {
        this.type = type;
    }
}
