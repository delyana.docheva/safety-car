package com.telerikacademy.safetycar.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AuthorityId implements Serializable {

    @Column(name = "user_id")
    private int userId;
    @Column(name = "authority")
    private String authority;

    public AuthorityId() {
    }

    public AuthorityId(int userId, String authority) {
        this.userId = userId;
        this.authority = authority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorityId that = (AuthorityId) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(authority, that.authority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, authority);
    }


    public int getUserId() {
        return userId;
    }

    public String getAuthority() {
        return authority;
    }
}
