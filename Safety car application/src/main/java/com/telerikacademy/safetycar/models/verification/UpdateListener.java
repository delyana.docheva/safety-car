package com.telerikacademy.safetycar.models.verification;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.UUID;

public class UpdateListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private IUserService service;
    private MessageSource messages;
    private JavaMailSender mailSender;

    @Autowired
    public UpdateListener(IUserService service, @Qualifier("messageSource") MessageSource messages, JavaMailSender mailSender) {
        this.service = service;
        this.messages = messages;
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createVerificationToken(user, token);

        String recipientAddress = user.getUsername();
        String subject = "Update Email Confirmation";
        String message = String.format("Dear %s, please confirm email update for your profile on our site by clicking the link bellow.", user.getUsername());
        String confirmationUrl
                = event.getAppUrl() + "/confirm-update?token=" + token;

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + "\r\n" + "http://localhost:8080" + confirmationUrl);
        mailSender.send(email);
    }
}
