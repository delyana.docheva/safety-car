package com.telerikacademy.safetycar.models;

import com.telerikacademy.safetycar.models.enums.PaymentStatusType;

import javax.persistence.*;

@Entity
@Table(name = "payment_status")
public class PaymentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private PaymentStatusType type;

    public PaymentStatus() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PaymentStatusType getType() {
        return type;
    }

    public void setType(PaymentStatusType type) {
        this.type = type;
    }
}
