package com.telerikacademy.safetycar.models.enums;

public enum CoefficientType {
    TAX_COEFF,
    ACCIDENT_COEFF,
    DRIVER_AGE_COEFF,
    PAYMENT_METHOD_COEFF,
    MIN_DRIVER_AGE_FOR_DISCOUNT;
}
