package com.telerikacademy.safetycar.models.verification;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.services.EmailService;
import com.telerikacademy.safetycar.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class PasswordResetListener implements ApplicationListener<OnPasswordResetEvent> {
    private IUserService service;
    private final EmailService emailService;

    @Autowired
    public PasswordResetListener(IUserService service, EmailService emailService) {
        this.service = service;
        this.emailService = emailService;
    }

    @Override
    public void onApplicationEvent(OnPasswordResetEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnPasswordResetEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createPasswordResetToken(user, token);

        emailService.generatePasswordResetMail(event, user, token);
    }

}
