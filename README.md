![Safety Car Logo](/Images/LogoSC.png) 
# Safety Car web application

## **Project Description**
Welcome to Safety Car - a web application where users get a  car insurance offer, save offers, request insurance policies,
 and keep track of their insurance policy history. 

* Technologies used:
    - [x] Java 11
    - [x] IDE: IntelliJ IDEA
    - [x] Database: SQL & MariaDB
    - [x] Spring 
    - [x] Hibernate
    - [x] Thymeleaf template engine
    - [x] HTML and CSS with Bootstrap
    - [x] Swagger 

Home URL:  http://localhost:8080 <br>
To view Swagger documentation: http://localhost:8080/swagger-ui/
To view Trello board: https://trello.com/b/IRUegPXh
<br/><br/>
* Areas:
    - public part (accessible without authentication) - users can visit the home page of the application and
     simulate an offer by filling out a form and getting an expected premium amount, calculated according to the input data. 
     Users can choose to save the offer, upon which they are required to log in or register. Registration requires users to 
     input a valid email address and activate their account by clicking on an activation link received by email.
     
    - private part (available for registered users) - users can simulate offers and save them in their profile. By visiting the
    My policies page, logged users can view all saved offers and requested policies. Logged users can request a policy by filling out a 
    form. Requested policies can be cancelled as long as their status is not marked as "pending" by an insurance agent. 
    Users receive an email as soon as an agent changes the status of their requested policy. Users are also emailed whenever 
    their policy is about to become active or expire. 
    
    - agent part (available for insurance agents and admin users) - insurance agents can manage insurance requests by visiting the Manage policies page where all requested policies
    are displayed. Policies can be filtered by different criteria such as premium amount, date of the request, and others. Agents can view details about each request and mark it as
    pending, approve it, or reject it. 
    
    - administration part (available for admin users) -  admin users can manage all users through the Manage users page, 
    where they can search for users by email or phone. They can view details about each user and edit their profile. 
    Admin users can change the roles of other users and promote them to agents or admins.   
   

|**Team #4** | |
|:------------:|:------------:|
|[**Delyana Docheva**](https://gitlab.com/delyana.docheva)<br>| [**Asya Lobutova**](https://gitlab.com/A.Lobutova)|

|![](/Images/Telerik_academy.png)|![](/Images/TINQIN-logo.png)|
|:------------:|:------------:|
|[**Telerik Academy**](https://www.telerikacademy.com/)|[**Tinqin**](http://tinqin.com/)|
