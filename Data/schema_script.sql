create schema safety_car;

create table safety_car.calculation_base_amount
(
    id          int auto_increment
        primary key,
    cc_min      int          not null,
    cc_max      int          not null,
    car_age_min int          not null,
    car_age_max int          not null,
    base_amount double(6, 2) not null
);

create table car_brand
(
    id   int auto_increment
        primary key,
    name varchar(20) not null,
    constraint car_brand_name_uindex
        unique (name)
);

create table safety_car.car_model
(
    id       int auto_increment
        primary key,
    brand_id int         not null,
    name     varchar(30) not null,
    constraint unique_index
        unique (brand_id, name),
    constraint car_model_car_brand_id_fk
        foreign key (brand_id) references safety_car.car_brand (id)
);

create table safety_car.coefficients
(
    id    int auto_increment
        primary key,
    name  varchar(40)  not null,
    value double(4, 2) not null,
    constraint coefficients_name_uindex
        unique (name)
);

create table status
(
    id   int auto_increment
        primary key,
    name varchar(20) not null,
    constraint status_name_uindex
        unique (name)
);

create table safety_car.country
(
    id   int auto_increment
        primary key,
    name varchar(50) not null,
    constraint countries_name_uindex
        unique (name)
);

create table safety_car.district
(
    id         int auto_increment
        primary key,
    country_id int         not null,
    name       varchar(30) not null,
    constraint district_name_uindex
        unique (name),
    constraint districts_country_id_fk
        foreign key (country_id) references safety_car.country (id)
);

create table safety_car.city
(
    id          int auto_increment
        primary key,
    district_id int default 1 not null,
    name        varchar(50)   not null,
    constraint cities_name_uindex
        unique (name, district_id),
    constraint city_districts_id_fk
        foreign key (district_id) references safety_car.district (id)
);

create table safety_car.users
(
    id           int auto_increment
        primary key,
    username     varchar(50)             not null,
    password     varchar(70)             not null,
    enabled      tinyint(1)   default 0  not null,
    first_name   varchar(20)  default '' null,
    last_name    varchar(20)  default '' null,
    birth_date   date                    null,
    phone        varchar(20)  default '' null,
    address      varchar(100) default '' null,
    city_id      int                     null,
    has_accident tinyint(1)   default 0  not null,
    is_deleted   tinyint(1)   default 0  not null,
    constraint users_username_uindex
        unique (username),
    constraint users_city_id_fk
        foreign key (city_id) references safety_car.city (id)
);

create table safety_car.authorities
(
    user_id   int         not null,
    username  varchar(50) not null,
    authority varchar(20) null,
    constraint authorities_pk
        unique (user_id, authority),
    constraint authorities_users_id_fk
        foreign key (user_id) references safety_car.users (id)
);

create table safety_car.car
(
    id                                      int auto_increment
        primary key,
    model_id                                int                  not null,
    cubic_capacity                          int                  not null,
    first_registration_date                 date                 not null,
    owner_id                                int                  not null,
    registration_certificate_image          mediumblob           null,
    registration_certificate_image_filetype varchar(100)          null,
    registration_number                     varchar(8)          null,
    is_deleted                              tinyint(1) default 0 not null,
    constraint car_registration_number_uindex
        unique (registration_number),
    constraint car_car_model_id_fk
        foreign key (model_id) references safety_car.car_model (id),
    constraint car_users_id_fk
        foreign key (owner_id) references safety_car.users (id)
);

create table safety_car.payment_method
(
    id   int auto_increment
        primary key,
    name varchar(20) default 'total amount' not null,
    constraint payment_method_name_uindex
        unique (name)
);

create table safety_car.payment_status
(
    id   int auto_increment
        primary key,
    name varchar(15) not null,
    constraint payment_status_name_uindex
        unique (name)
);

create table safety_car.insurance_policy
(
    id                int auto_increment
        primary key,
    car_id            int           not null,
    saved_date        datetime      not null,
    premium_amount    double(6, 2)  not null,
    status_id         int default 1 not null,
    owner_id          int           null,
    start_date        date          null,
    expiring_date     date          null,
    payment_status_id int           null,
    requested_date    datetime      null,
    payment_method_id int           null,
    agent_id          int           null,
    constraint insurance_policy_car_id_fk
        foreign key (car_id) references safety_car.car (id),
    constraint insurance_policy_payment_method_id_fk
        foreign key (payment_method_id) references safety_car.payment_method (id),
    constraint insurance_policy_payment_status_id_fk
        foreign key (payment_status_id) references safety_car.payment_status (id),
    constraint insurance_policy_status_id_fk
        foreign key (status_id) references safety_car.status (id),
    constraint insurance_policy_users_id_fk
        foreign key (owner_id) references safety_car.users (id),
    constraint insurance_policy_users_id_fk_2
        foreign key (agent_id) references safety_car.users (id)
);

create table verification_token
(
    id          int auto_increment
        primary key,
    token       varchar(50) not null,
    user_id     int         not null,
    expiry_date date        not null,
    constraint verification_token_token_uindex
        unique (token)
);